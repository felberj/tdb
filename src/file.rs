use error::*;
use std::fs::File;
use std::io::prelude::*;
use std::rc::Rc;
use structs::Struct;

#[derive(Default)]
pub struct System {
    files: Vec<Rc<Node>>,
    fds: Vec<Fd>,
    next_fd: u64,
    next_ino: u64,
}

#[derive(Default)]
pub struct Stat {
    pub readable: bool,
    pub writable: bool,
    pub executable: bool,
    pub size: usize,
    pub ino: u64,
}

impl Stat {
    pub fn to_stat(&self) -> Struct {
        let mut mode = 0;
        if self.writable {
            mode |= 0100;
        }
        if self.readable {
            mode |= 0200;
        }
        if self.executable {
            mode |= 0400;
        }

        let mut s: Vec<Struct> = Vec::new();
        s.push(Struct::U64("st_dev", self.ino));
        s.push(Struct::Uword("st_ino", self.ino));
        s.push(Struct::Uword("st_nlink", 1));
        s.push(Struct::U32("st_mode", mode));
        s.push(Struct::U32("st_uid", 1000));
        s.push(Struct::U32("st_gid", 5));
        s.push(Struct::U64("st_rdev", 34816));
        s.push(Struct::Uword("st_size", self.size as u64));
        s.push(Struct::Uword("st_blksize", 1024));
        s.push(Struct::Uword("st_blocks", 0));
        // TODO add time
        Struct::Struct("", s)
    }
}

pub struct Node {
    pub content: Vec<u8>,
    pub path: &'static str,
    pub stat: Stat,
}

pub struct Fd {
    pub nr: u64,
    pub offset: usize,
    pub node: Rc<Node>,
}

impl PartialEq for Fd {
    fn eq(&self, other: &Fd) -> bool {
        self.nr == other.nr
    }
}

impl System {
    pub fn new() -> System {
        let mut sys: System = Default::default();
        sys.put(Node {
            content: Vec::new(),
            path: "stdin",
            stat: Stat {
                writable: true,
                ..Default::default()
            },
        });
        sys.open("stdin").unwrap();
        sys.put(Node {
            content: Vec::new(),
            path: "stdout",
            stat: Stat {
                writable: true,
                ..Default::default()
            },
        });
        sys.open("stdout").unwrap();
        sys.put(Node {
            content: Vec::new(),
            path: "stderr",
            stat: Stat {
                readable: true,
                ..Default::default()
            },
        });
        sys.open("stderr").unwrap();
        sys
    }

    pub fn get_fd<'a>(&'a self, nr: u64) -> Option<&'a Fd> {
        for f in &self.fds {
            if f.nr == nr {
                return Some(&f);
            }
        }
        None
    }

    fn get_fd_mut<'a>(&'a mut self, nr: u64) -> Option<&'a mut Fd> {
        for f in &mut self.fds {
            if f.nr == nr {
                return Some(f);
            }
        }
        None
    }

    pub fn fstat(&self, fd: u64) -> Result<Stat> {
        match fd {
            1 => {
                // stdout
                return Ok(Stat {
                    readable: false,
                    writable: true,
                    executable: false,
                    size: 0,
                    ..Default::default()
                });
            }
            _ => {
                match self.get_fd(fd) {
                    Some(fd) => {
                        let node = &fd.node;
                        return Ok(Stat {
                            readable: node.stat.readable,
                            writable: node.stat.writable,
                            executable: node.stat.executable,
                            size: node.content.len(),
                            ino: node.stat.ino,
                        });
                    }
                    None => {}
                }
                bail!{"invalid file descriptor {}", fd};
            }
        }
    }

    pub fn open(&mut self, path: &str) -> Result<&Fd> {
        // TODO make better errors (access, already exists, ...)
        let fd = match self.get(path) {
            Some(f) => {
                // TODO make check for writable, ...
                let fd = self.next_fd;
                Fd {
                    nr: fd,
                    offset: 0,
                    node: f,
                }
            }
            None => bail!{"not found"},
        };
        self.next_fd += 1;
        self.fds.push(fd);
        let ref fd = self.fds.last().unwrap();
        return Ok(&fd);
    }

    pub fn close(&mut self, fd: u64) {
        let mut i: usize = 0;
        for f in &self.fds {
            if f.nr == fd {
                break;
            }
            i += 1;
        }
        if i == self.fds.len() {
            return;
        }
        self.fds.remove(i);
    }

    pub fn read(&mut self, fd: u64, len: usize) -> Result<Vec<u8>> {
        match self.get_fd_mut(fd) {
            Some(fd) => {
                let offset = fd.offset;
                let mut len = len;
                let data = fd.node.content.as_slice();
                let mut result = Vec::with_capacity(len);
                if offset > data.len() {
                    return Ok(result);
                }
                if offset + len > data.len() {
                    len = data.len() - offset;
                }
                result.extend_from_slice(&data[offset..(offset + len)]);
                fd.offset = offset + len;
                return Ok(result);
            }
            None => {}
        }
        bail!{"invalid fd"};
    }

    pub fn put(&mut self, node: Node) {
        // TODO check for dublicates
        let mut node = node;
        self.next_ino += 1;
        node.stat.ino = self.next_ino;
        self.files.push(Rc::new(node));
    }

    pub fn get(&self, path: &str) -> Option<Rc<Node>> {
        for ref file in &self.files {
            if path == file.path {
                return Some(Rc::clone(file));
            }
        }
        None
    }

    pub fn load_real_file(
        &mut self,
        real_path: &str,
        path: &'static str,
        stat: Stat,
    ) -> Result<()> {
        let mut file = File::open(real_path)?;
        let mut content: Vec<u8> = Vec::new();
        file.read_to_end(&mut content)?;
        self.put(Node {
            path: path,
            stat: stat,
            content: content,
        });
        Ok(())
    }
}
