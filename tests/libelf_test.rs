extern crate tdb;

use binary::{Bits, Ehdr, ElfIdent, ElfType, Endianness, Machine, Phdr, SegmentType, parse_ehdr, parse_program_headers};

use libtest::T;

use std::fs::File;
use std::io::prelude::*;
use tdb::*;

#[test]
fn parse_ehdr_test() {
    let mut t: T = Default::default();
    struct TestCase {
        file: &'static str,
        want: Ehdr,
    }
    // FIXME(felber) not happy with the formatting below, but well
    let tests = [TestCase { file: "testdata/elf_x86_64_keyval.out", want: Ehdr { e_type: ElfType::Exec, e_ident: ElfIdent { bits: Bits::Bits64, endianness: Endianness::Big }, e_entry: 0x400850, e_phoff: 64, e_shoff: 11776, e_phentsize: 56, e_phnum: 9, e_shnum: 30, e_shentsize: 0x40, machine: Machine::X86_64 } },
                 TestCase { file: "testdata/static/static_x86", want: Ehdr { e_type: ElfType::Exec, e_ident: ElfIdent { bits: Bits::Bits32, endianness: Endianness::Big }, e_entry: 0x804887f, e_phoff: 52, e_shoff: 726712, e_phentsize: 32, e_phnum: 6, e_shnum: 31, e_shentsize: 0x28, machine: Machine::X86 } },
                 TestCase { file: "testdata/static/static_x86_64", want: Ehdr { e_type: ElfType::Exec, e_ident: ElfIdent { bits: Bits::Bits64, endianness: Endianness::Big }, e_entry: 0x400990, e_phoff: 64, e_shoff: 808416, e_phentsize: 56, e_phnum: 6, e_shnum: 32, e_shentsize: 0x40, machine: Machine::X86_64 } },
                 TestCase { file: "testdata/static/dynamic_x86_64_print_args", want: Ehdr { e_type: ElfType::Exec, e_ident: ElfIdent { bits: Bits::Bits64, endianness: Endianness::Big }, e_entry: 0x400440, e_phoff: 64, e_shoff: 6608, e_phentsize: 56, e_phnum: 9, e_shnum: 30, e_shentsize: 0x40, machine: Machine::X86_64 } },
                 TestCase { file: "testdata/lib64/ld-linux-x86-64.so.2", want: Ehdr { e_type: ElfType::Dyn, e_ident: ElfIdent { bits: Bits::Bits64, endianness: Endianness::Big }, e_entry: 0xcd0, e_phoff: 64, e_shoff: 152776, e_phentsize: 56, e_phnum: 7, e_shnum: 25, e_shentsize: 0x40, machine: Machine::X86_64 } }];
    for test in tests.iter() {
        let mut file = File::open(test.file).unwrap();
        let mut contents: Vec<u8> = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        let ehdr = parse_ehdr(&contents).unwrap();
        if test.want.e_type != ehdr.e_type {
            t.error(format!("parse_ehdr({}).e_type: got {:?}, expected {:?}", test.file, test.want.e_type, ehdr.e_type));
        }
        if test.want.e_ident.bits != ehdr.e_ident.bits {
            t.error(format!("parse_ehdr({}).e_ident.bits: got {:?}, expected {:?}", test.file, test.want.e_ident.bits, ehdr.e_ident.bits));
        }
        if test.want.e_entry != ehdr.e_entry {
            t.error(format!("parse_ehdr({}).e_entry: got {:?}, expected {:?}", test.file, test.want.e_entry, ehdr.e_entry));
        }
        if test.want.e_phoff != ehdr.e_phoff {
            t.error(format!("parse_ehdr({}).e_phoff: got {:?}, expected {:?}", test.file, test.want.e_phoff, ehdr.e_phoff));
        }
        if test.want.e_shoff != ehdr.e_shoff {
            t.error(format!("parse_ehdr({}).e_shoff: got {:?}, expected {:?}", test.file, test.want.e_shoff, ehdr.e_shoff));
        }
        if test.want.e_phentsize != ehdr.e_phentsize {
            t.error(format!("parse_ehdr({}).e_phentsize: got {:?}, expected {:?}", test.file, test.want.e_phentsize, ehdr.e_phentsize));
        }
        if test.want.e_phnum != ehdr.e_phnum {
            t.error(format!("parse_ehdr({}).e_phnum: got {:?}, expected {:?}", test.file, test.want.e_phnum, ehdr.e_phnum));
        }
        if test.want.e_shnum != ehdr.e_shnum {
            t.error(format!("parse_ehdr({}).e_shnum: got {:?}, expected {:?}", test.file, test.want.e_shnum, ehdr.e_shnum));
        }
        // todo test machine
    }
}

#[test]
fn parse_phdr_test() {
    // todo test loader/interpreter
    let mut t: T = Default::default();
    struct TestCase {
        file: &'static str,
        want: Vec<Phdr>,
    }
    let tests = [TestCase {
                     file: "testdata/elf_x86_64_keyval.out",
                     want: vec![Phdr { p_type: SegmentType::Phdr, p_offset: 0x0000000000000040, p_filesz: 0x00000000000001f8, p_vaddr: 0x0000000000400040, p_memsz: 0x00000000000001f8, p_align: 0x8, readable: true, writable: false, executable: true },
                                Phdr { p_type: SegmentType::Interp, p_offset: 0x0000000000000238, p_filesz: 0x000000000000001c, p_vaddr: 0x0000000000400238, p_memsz: 0x000000000000001c, p_align: 0x1, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::Load, p_offset: 0x0000000000000000, p_filesz: 0x000000000000135c, p_vaddr: 0x0000000000400000, p_memsz: 0x000000000000135c, p_align: 0x200000, readable: true, writable: false, executable: true },
                                Phdr { p_type: SegmentType::Load, p_offset: 0x0000000000001e10, p_filesz: 0x0000000000000290, p_vaddr: 0x0000000000601e10, p_memsz: 0x00000000000002b8, p_align: 0x200000, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Dynamic, p_offset: 0x0000000000001e28, p_filesz: 0x00000000000001d0, p_vaddr: 0x0000000000601e28, p_memsz: 0x00000000000001d0, p_align: 0x8, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Note, p_offset: 0x0000000000000254, p_filesz: 0x0000000000000044, p_vaddr: 0x0000000000400254, p_memsz: 0x0000000000000044, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuEhFrame, p_offset: 0x00000000000010c8, p_filesz: 0x000000000000007c, p_vaddr: 0x00000000004010c8, p_memsz: 0x000000000000007c, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuStack, p_offset: 0x0000000000000000, p_filesz: 0x0000000000000000, p_vaddr: 0x0000000000000000, p_memsz: 0x0000000000000000, p_align: 0x10, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::GnuRelro, p_offset: 0x0000000000001e10, p_filesz: 0x00000000000001f0, p_vaddr: 0x0000000000601e10, p_memsz: 0x00000000000001f0, p_align: 0x1, readable: true, writable: false, executable: false }],
                 },
                 TestCase {
                     file: "testdata/static/static_x86",
                     want: vec![Phdr { p_type: SegmentType::Load, p_offset: 0x000000, p_filesz: 0xa15f3, p_vaddr: 0x08048000, p_memsz: 0xa15f3, p_align: 0x1000, readable: true, writable: false, executable: true },
                                Phdr { p_type: SegmentType::Load, p_offset: 0x0a1f5c, p_filesz: 0x01024, p_vaddr: 0x080eaf5c, p_memsz: 0x01e48, p_align: 0x1000, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Note, p_offset: 0x0000f4, p_filesz: 0x00044, p_vaddr: 0x080480f4, p_memsz: 0x00044, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::Tls, p_offset: 0x0a1f5c, p_filesz: 0x00010, p_vaddr: 0x080eaf5c, p_memsz: 0x00028, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuStack, p_offset: 0x000000, p_filesz: 0x00000, p_vaddr: 0x00000000, p_memsz: 0x00000, p_align: 0x10, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::GnuRelro, p_offset: 0x0a1f5c, p_filesz: 0x000a4, p_vaddr: 0x080eaf5c, p_memsz: 0x000a4, p_align: 0x1, readable: true, writable: false, executable: false }],
                 },
                 TestCase {
                     file: "testdata/static/static_x86_64",
                     want: vec![Phdr { p_type: SegmentType::Load, p_offset: 0x0000000000000000, p_filesz: 0x00000000000b10db, p_vaddr: 0x0000000000400000, p_memsz: 0x00000000000b10db, p_align: 0x200000, readable: true, writable: false, executable: true },
                                Phdr { p_type: SegmentType::Load, p_offset: 0x00000000000b1eb8, p_filesz: 0x0000000000001c98, p_vaddr: 0x00000000006b1eb8, p_memsz: 0x0000000000003570, p_align: 0x200000, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Note, p_offset: 0x0000000000000190, p_filesz: 0x0000000000000044, p_vaddr: 0x0000000000400190, p_memsz: 0x0000000000000044, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::Tls, p_offset: 0x00000000000b1eb8, p_filesz: 0x0000000000000020, p_vaddr: 0x00000000006b1eb8, p_memsz: 0x0000000000000050, p_align: 0x8, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuStack, p_offset: 0x0000000000000000, p_filesz: 0x0000000000000000, p_vaddr: 0x0000000000000000, p_memsz: 0x0000000000000000, p_align: 0x10, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::GnuRelro, p_offset: 0x00000000000b1eb8, p_filesz: 0x0000000000000148, p_vaddr: 0x00000000006b1eb8, p_memsz: 0x0000000000000148, p_align: 0x1, readable: true, writable: false, executable: false }],
                 },
                 TestCase {
                     file: "testdata/lib64/ld-linux-x86-64.so.2",
                     want: vec![Phdr { p_type: SegmentType::Load, p_offset: 0x0000000000000000, p_filesz: 0x0000000000023420, p_vaddr: 0x0000000000000000, p_memsz: 0x0000000000023420, p_align: 0x200000, readable: true, writable: false, executable: true },
                                Phdr { p_type: SegmentType::Load, p_offset: 0x0000000000023c00, p_filesz: 0x00000000000013c4, p_vaddr: 0x0000000000223c00, p_memsz: 0x0000000000001588, p_align: 0x200000, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Dynamic, p_offset: 0x0000000000023e70, p_filesz: 0x0000000000000170, p_vaddr: 0x0000000000223e70, p_memsz: 0x0000000000000170, p_align: 0x8, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::Note, p_offset: 0x00000000000001c8, p_filesz: 0x0000000000000024, p_vaddr: 0x00000000000001c8, p_memsz: 0x0000000000000024, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuEhFrame, p_offset: 0x0000000000020a24, p_filesz: 0x0000000000000634, p_vaddr: 0x0000000000020a24, p_memsz: 0x0000000000000634, p_align: 0x4, readable: true, writable: false, executable: false },
                                Phdr { p_type: SegmentType::GnuStack, p_offset: 0x0000000000000000, p_filesz: 0x0000000000000000, p_vaddr: 0x0000000000000000, p_memsz: 0x0000000000000000, p_align: 0x10, readable: true, writable: true, executable: false },
                                Phdr { p_type: SegmentType::GnuRelro, p_offset: 0x0000000000023c00, p_filesz: 0x0000000000000400, p_vaddr: 0x0000000000223c00, p_memsz: 0x0000000000000400, p_align: 0x1, readable: true, writable: false, executable: false }],
                 }];
    for test in tests.iter() {
        let mut file = File::open(test.file).unwrap();
        let mut contents: Vec<u8> = Vec::new();
        file.read_to_end(&mut contents).unwrap();
        let ehdr = parse_ehdr(&contents).unwrap();
        let phdrs = parse_program_headers(&contents, &ehdr).unwrap();
        if test.want.len() != phdrs.len() {
            t.error(format!("parse_program_headers({}).len(), expected {}, got {}", test.file, test.want.len(), phdrs.len()));
            continue;
        }
        for (want, got) in test.want.iter().zip(phdrs.iter()) {
            if want.p_type != got.p_type {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_type, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_type,
                                got.p_type));
            }
            if want.p_offset != got.p_offset {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_offset, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_offset,
                                got.p_offset));
            }
            if want.p_filesz != got.p_filesz {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_filesz, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_filesz,
                                got.p_filesz));
            }
            if want.p_vaddr != got.p_vaddr {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_vaddr, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_vaddr,
                                got.p_vaddr));
            }
            if want.p_memsz != got.p_memsz {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_memsz, expected  \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_memsz,
                                got.p_memsz));
            }
            if want.p_align != got.p_align {
                t.error(format!("parse_program_headers({}), offset:{} wrong p_align, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.p_align,
                                got.p_align));
            }
            if want.writable != got.writable {
                t.error(format!("parse_program_headers({}), offset:{} wrong writable, expected \
                                 {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.writable,
                                got.writable));
            }
            if want.executable != got.executable {
                t.error(format!("parse_program_headers({}), offset:{} wrong executable, \
                                 expected {:?}, got {:?}",
                                test.file,
                                want.p_offset,
                                want.executable,
                                got.executable));
            }
        }
    }
}
