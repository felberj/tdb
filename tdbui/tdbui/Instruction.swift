//
//  Instruction.swift
//  tdbui
//
//  Created by Jonas Felber on 20.01.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

class Instruction: NSObject {
    @objc dynamic var time: UInt64
    @objc dynamic var address: UInt64
    @objc dynamic var instruction: String
    @objc dynamic var registers: [Register]
    
    init(time: UInt64, address: UInt64, instruction: String, registers: [Register]) {
        self.time = time
        self.address = address
        self.instruction = instruction
        self.registers = registers
        super.init()
    }
}
