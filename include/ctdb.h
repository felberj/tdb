
#ifndef cheddar_generated_ctdb_h
#define cheddar_generated_ctdb_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>



typedef struct TdbInstruction {
	uint64_t time;
	uint64_t address;
	char const* op_str;
	Instruction_ const* ins_handle;
} TdbInstruction;

typedef void* CallbackHandle;

typedef void (*InstructionExecutedCallback)(CallbackHandle , TdbInstruction );

typedef void (*CollectRegistersCallback)(void* , char const* , uint64_t );

typedef void (*SyscallExecutedCallback)(CallbackHandle , uint64_t , char const* );

void libtdb_options_set_instruction_executed_callback(Options* ptr, void* handle, InstructionExecutedCallback cb);

void libtdb_instruction_collect_registers(void* user_data, TdbInstruction instr, CollectRegistersCallback cb);

void libtdb_options_set_syscall_executed_callback(Options* ptr, void* handle, SyscallExecutedCallback cb);

Options* libtdb_options_new(void);

void libtdb_options_add_program_argument(Options* ptr, char const* option);

Tdb* libtdb_tdb_new(Elf const* elf, Options* opt, int* ok);

int libtdb_tdb_start(Tdb* tdb);

int libtdb_tdb_load_elf(Tdb* tdb, Elf const* elf);



#ifdef __cplusplus
}
#endif


#endif
