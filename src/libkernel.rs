use binary::{Bits, Endian};
use error::*;
use libload::Process;
use libtdb::Cpu;
use std::collections;
use std::str;
use std::time::{SystemTime, UNIX_EPOCH};
use structs::Struct;
use unicorn;
use unicorn::{RegisterX86, X86MMR, X86MSR};

pub mod gdt {
    // http://wiki.osdev.org/Global_Descriptor_Table
    pub const A_PRESENT: u8 = 1 << 7; // Present bit. This must be 1 for all valid selectors.
    pub const A_PRIV_0: u8 = 0 << 5; // Kernel privilege
    pub const A_PRIV_3: u8 = 3 << 5; // User privilege
    pub const A_EXEC: u8 = 1 << 3; // Executable bit, TODO value wrong?
    pub const A_DATA: u8 = 0x10; // Data segment (not executable), TODO value wrong?
    pub const A_GROW_UP: u8 = 0; // Segment grows up
    pub const A_GROW_DOWN: u8 = 1 << 2; // Segment grows down
    pub const A_WRITE: u8 = 1 << 1;
    pub const A_UNKNOWN: u8 = 1 << 4; // always 1?!
    pub const F_GRANULARITY_BYTE: u8 = 0; // limit is in 1 B blocks (byte granularity)
    pub const F_GRANULARITY_PAGE: u8 = 1 << 7; // limit is in 4 KiB blocks (page granularity).
    pub const F_SZ_32: u8 = 1 << 6; // 32 bit protected mode
    pub const S_PRIV_0: u8 = 0x0;
    pub const S_PRIV_3: u8 = 0x3;
}

pub mod flags {
    pub mod open {
        pub const RD_ONLY: u64 = 00;
        pub const WR_ONLY: u64 = 01;
        pub const RDWR: u64 = 02; // read write
        pub const CLOEXEC: u64 = 02000000; // set close_on_exec TODO no clue what it does
    }
    pub mod mmap {
        pub const PROT_READ: u64 = 1;
        pub const PROT_WRITE: u64 = 2;
        pub const PROT_EXEC: u64 = 4;
    }
}

const MINUS_ONE: u64 = 0xffffffffffffffff;

pub struct Syscall {
    pub name: String,
    pub syscall: Box<Fn(&mut Process, &mut Cpu, [u64; 6]) -> Result<u64>>,
}

fn read_string<'a>(cpu: &mut Cpu, addr: u64, buf: &'a mut Vec<u8>) -> Result<&'a str> {
    buf.clear();
    let mut data = cpu.mem_read(addr, 255)?;
    buf.append(&mut data);
    let zero_byte = match buf.iter().position(|&x| x == 0) {
        Some(index) => index,
        None => bail!{"file too long: no zero byte found"},
    };
    match str::from_utf8(&buf.as_slice()[..zero_byte]) {
        Ok(res) => Ok(res),
        _ => bail!{"UTF8 Error"},
    }
}

fn write_string(cpu: &mut Cpu, addr: u64, s: &str) -> Result<()> {
    let data = s.as_bytes();
    let mut vec: Vec<u8> = Vec::with_capacity(data.len() + 1);
    vec.extend_from_slice(data);
    vec.push(0); // terminating zero byte
    cpu.mem_write(addr, vec.as_slice())?;
    Ok(())
}

fn append_and_pad(buf: &mut Vec<u8>, value: &str, pad: usize) -> Result<()> {
    let bytes = value.as_bytes();
    // +1 because of the null termitator
    if bytes.len() + 1 > pad {
        bail!{"unable to append and pad string to the buffer: string is too long"}
    }
    buf.extend_from_slice(bytes);
    for _i in 0..(pad - bytes.len()) {
        buf.push(0);
    }
    Ok(())
}

fn sys_mmap2(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let mut addr = args[0];
    let length = args[1] as usize;
    let mut prot_flags = args[2];
    let flags = args[3];
    let fd = args[4] as i32;
    let pgoffset = args[5]; //TODO
    println!{"sys_mmap2(0x{:x}, {}, {}, {}, {}, {})", addr, length, prot_flags, flags, fd, pgoffset};
    let mut prot = unicorn::PROT_NONE;

    if prot_flags & flags::mmap::PROT_READ != 0 {
        prot |= unicorn::PROT_READ;
        prot_flags ^= flags::mmap::PROT_READ;
    }
    if prot_flags & flags::mmap::PROT_WRITE != 0 {
        prot |= unicorn::PROT_WRITE;
        prot_flags ^= flags::mmap::PROT_WRITE;
    }
    if prot_flags & flags::mmap::PROT_EXEC != 0 {
        prot |= unicorn::PROT_EXEC;
        prot_flags ^= flags::mmap::PROT_EXEC;
    }

    if prot_flags != 0 {
        bail!{"Unused protection flags {}", prot_flags};
    }

    if addr != 0 {
        addr = p.allocate_memory(cpu, Some(addr), length as u64, prot, "")?;
    } else {
        addr = p.allocate_memory(cpu, None, length as u64, prot, "")?;
    }
    if fd != -1 {
        if flags & 0x02 == 0 {
            // TODO flag NOT_PRIVATE not set
            bail!{"no filemappings without MAP_PRIVATE supported yet"};
        }
        match p.fs.get_fd(fd as u64) {
            Some(fd) => {
                let mut content = fd.node.content.as_slice();
                if content.len() > length {
                    content = &content[0..length];
                }
                cpu.mem_write(addr, content)?;
            }
            None => bail!{"fd for mmap not found"},
        }
    }

    Ok(addr)
}

fn sys_close(p: &mut Process, _: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let fd = args[0];
    println!{"sys_close({})", fd};
    p.fs.close(fd);
    Ok(0)
}

fn sys_mprotect(_: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let addr = args[0];
    let size = args[1] as usize;
    let mut prot_flags = args[2];
    println!{"sys_mprotect(0x{:x}, {}, {})", addr, size, prot_flags};

    let mut prot = unicorn::PROT_NONE;
    if prot_flags & flags::mmap::PROT_READ != 0 {
        prot |= unicorn::PROT_READ;
        prot_flags ^= flags::mmap::PROT_READ;
    }
    if prot_flags & flags::mmap::PROT_WRITE != 0 {
        prot |= unicorn::PROT_WRITE;
        prot_flags ^= flags::mmap::PROT_WRITE;
    }
    if prot_flags & flags::mmap::PROT_EXEC != 0 {
        prot |= unicorn::PROT_EXEC;
        prot_flags ^= flags::mmap::PROT_EXEC;
    }

    if prot_flags != 0 {
        bail!{"Unused protection flags {}", prot_flags};
    }

    cpu.mem_protect(addr, size, prot)?;

    Ok(0)
}

// http://pubs.opengroup.org/onlinepubs/009696899/basedefs/sys/utsname.h.html
fn sys_uname(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let addr = args[0];
    println!{"sys_uname(0x{:x})", addr};
    let mut buf: Vec<u8> = Vec::new();
    append_and_pad(&mut buf, "Linux", 65)?; // sysname
    append_and_pad(&mut buf, "tdb_nodename", 65)?; // nodename
    append_and_pad(&mut buf, "3.13.0-24-generic", 65)?; // release
    append_and_pad(&mut buf, "tdb", 65)?; // version
    match p.bits {
        Bits::BITS_32 => append_and_pad(&mut buf, "x86", 65)?,
        Bits::BITS_64 => append_and_pad(&mut buf, "x86_64", 65)?,
    }
    cpu.mem_write(addr, buf.as_slice())?;
    Ok(0)
}

fn sys_brk(process: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    // calling brk with an invalid address returns the current value?
    let mut addr = args[0];
    println!{"sys_brk(0x{:x})", addr};
    if addr > 0 && addr >= process.brk {
        let rem = addr % 0x1000; // addr must be aligned
        if rem != 0 {
            addr += 0x1000 - rem;
        }
        let size = addr - process.brk;
        if size > 0 {
            let prot = unicorn::PROT_READ | unicorn::PROT_WRITE;
            // TODO check size, so it is not too big
            cpu.mem_map(process.brk, size as usize, prot)?;
        }
        process.brk = addr + size;
    }
    Ok(process.brk)
}

// X86_64 only
fn sys_arch_prctl(_: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let code = args[0];
    let addr = args[1];
    println!{"sys_arch_prctl({}, 0x{:x})", code, addr};
    let res = match code {
        // 0x1001 => cpu.reg_write(RegisterX86::GS, addr).and(Ok(0))?, // ARCH_SET_GS
        // TODO is this correct?!
        0x1002 => cpu.x86_msr_write(X86MSR::FS, addr).and(Ok(0))?, // ARCH_SET_FS
        // 0x1003 => cpu.reg_read(RegisterX86::FS)?, // ARCH_GET_FS
        // 0x1004 => cpu.reg_read(RegisterX86::GS)?, // ARCH_GET_GS
        //
        // THIS IS WRONG, I NEED TO SET IT ON ANOTHER PLACE
        // https://github.com/unicorn-engine/unicorn/blob/4b9efdc986616b6b7b70d7c188b267f95237931e/qemu/target-i386/unicorn.c
        // https://github.com/unicorn-engine/unicorn/pull/755
        _ => bail!{"invalid code {}", code}, // TODO return proper error code
    };
    Ok(res)
}

fn sys_open(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let mut buf: Vec<u8> = Vec::new();
    let file = read_string(cpu, args[0], &mut buf)?;
    println!{"sys_open({})", file};
    // TODO check the mode!
    match p.fs.open(file) {
        Ok(fd) => Ok(fd.nr),
        Err(_) => Ok(MINUS_ONE),
    }
}

fn sys_writev(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let fd = args[0];
    let iov = args[1];
    let iov_count = args[2];
    println!{"sys_writev({}, {:x}, {})", fd, iov, iov_count};
    let mut ptrs: Vec<Struct> = Vec::new();
    ptrs.push(Struct::Uword("data", 0));
    ptrs.push(Struct::Uword("len", 0));
    let mut iov_struct = Struct::Struct("", ptrs);

    let mut buf: Vec<u8> = Vec::new();
    let size = iov_struct.size(p.bits);
    for i in 0..iov_count {
        let ptr = iov + i * size as u64;
        let memory = cpu.mem_read(ptr, size)?;
        iov_struct.unpack(p.bits, p.endian, memory.as_slice())?;
        let data_ptr = iov_struct.get("data")?.to_u64()?;
        let data_len = iov_struct.get("len")?.to_u64()?;

        let mut data = cpu.mem_read(data_ptr, data_len as usize)?;
        buf.append(&mut data);
    }
    // TODO actually write it to the fd
    println!("TODO wrote {}", str::from_utf8(buf.as_slice())?);
    Ok(buf.len() as u64)
}

fn sys_readlink(_: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let mut buf: Vec<u8> = Vec::new();
    let file = read_string(cpu, args[0], &mut buf)?;
    let buff = args[1];
    println!{"sys_readlink({}, 0x{:x})", file, buff};
    match file {
        "/proc/self/exe" => {
            write_string(cpu, buff, "/bin/tdb")?;
            Ok(0)
        }
        _ => bail!{"sys_readlink not implemented {}", file},
    }
}

fn sys_access(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let mut buf: Vec<u8> = Vec::new();
    let file = read_string(cpu, args[0], &mut buf)?;
    let mode = args[1];
    println!{"sys_access({}, {})", file, mode};
    match p.fs.get(file) {
        Some(node) => {
            if mode & 0x01 != 0 && !node.stat.executable {
                // X_OK
                return Ok(MINUS_ONE);
            }
            if mode & 0x02 != 0 && !node.stat.writable {
                return Ok(MINUS_ONE);
            }
            if mode & 0x04 != 0 && !node.stat.readable {
                return Ok(MINUS_ONE);
            }
            return Ok(0);
        }
        None => Ok(MINUS_ONE), // TODO set errno
    }
}

fn sys_fstat(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let fd = args[0];
    let addr = args[1];
    println!{"sys_fstat({}, 0x{:x})", fd, addr};
    match p.fs.fstat(fd) {
        Ok(stat) => {
            let st = stat.to_stat();
            let data = st.pack(p.bits, Endian::LITTLE)?;
            cpu.mem_write(addr, data.as_slice())?;
            return Ok(0);
        }
        Err(_e) => {
            let err: i64 = -1;
            return Ok(err as u64);
        }
    }
}

fn sys_time(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let addr = args[0];
    println!{"sys_time(0x{:x})", addr};
    let start = SystemTime::now();
    let since_the_epoch = start.duration_since(UNIX_EPOCH)?.as_secs();
    let data = Struct::Uword("", since_the_epoch).pack(p.bits, Endian::LITTLE)?;
    cpu.mem_write(addr, data.as_slice())?;

    Ok(0)
}

// http://wiki.osdev.org/Global_Descriptor_Table
fn gdt_write(
    cpu: &mut Cpu,
    p: &mut Process,
    entry_number: u64,
    base: u64,
    limit: u64,
    flags: u8,
    access: u8,
) -> Result<()> {
    let mut limit = limit;
    let mut flags = flags;
    if limit > 0xfffff {
        // set page granularity
        limit >>= 12;
        flags |= gdt::F_GRANULARITY_PAGE;
    }
    let mut descriptor: u64 = limit & 0xffff;
    descriptor |= (base & 0xffffff) << 16;
    descriptor |= ((access as u64) & 0xff) << 40;
    descriptor |= ((limit >> 16) & 0xf) << 48;
    descriptor |= ((flags as u64) & 0xf0) << 48;
    descriptor |= ((base >> 24) & 0xff) << 56;

    let gdt = match p.gdt {
        Some(addr) => addr,
        None => {
            bail!{"Invalid state: No GDT set"};
        }
    };
    let data = Struct::U64("", descriptor).pack(p.bits, p.endian)?;
    // TODO is this correct?
    cpu.mem_write(gdt + (data.len() as u64) * entry_number, data.as_slice())?;
    Ok(())
}

fn sys_write(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let fd = args[0];
    let addr = args[1];
    let size = args[2] as usize;
    println!{"sys_write({}, 0x{:x}, {})", fd, addr, size}
    match fd {
        1 => {
            let data = cpu.mem_read(addr, size)?;
            let s = str::from_utf8(data.as_slice())?;
            p.stdout.push_str(s);
            println!("wrote to stdout {}", s);
        }
        _ => bail!{"sys_write not implemented for fd {}", args[0]},
    }
    Ok(args[2])
}

fn sys_exit_group(_: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    println!("sys_exit({})", args[0]);
    cpu.emu_stop()?;
    Ok(0)
}

fn sys_set_thread_area(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    // http://wiki.osdev.org/Global_Descriptor_Table
    // http://wiki.osdev.org/GDT_Tutorial
    let addr = args[0];
    println!{"sys_set_thread_area(0x{:x})", addr};

    let mut s: Vec<Struct> = Vec::new();
    s.push(Struct::I32("entry_number", 0));
    s.push(Struct::Uword("base_addr", 0));
    s.push(Struct::U32("limit", 0)); // TODO i32?
    s.push(Struct::I32("flags", 0));

    let mut data = Struct::Struct("", s);
    let size = data.size(p.bits);
    data.unpack(p.bits, p.endian, cpu.mem_read(addr, size)?.as_slice())?;
    let mut num = data.get("entry_number")?.to_i64()?;
    if num > 3 || num < -1 {
        // TODO log error
        return Ok(MINUS_ONE);
    }
    if num == -1 {
        for i in 1..2 {
            // TODO it only works for 0
            if !p.thread_areas.contains(&i) {
                data.get_mut("entry_number")?.set_i64(i)?;
                num = i;
                break;
            }
        }
        if num == -1 {
            // TODO log error
            return Ok(MINUS_ONE);
        }
        cpu.mem_write(args[0], data.pack(p.bits, p.endian)?.as_slice())?;
    }

    let limit = data.get("limit")?.to_u64()?;
    let flags = data.get("flags")?.to_i64()?;
    //  let flag_usable: bool = flags & (1 << 6);
    //  let flag_seg_not_present: bool = flags & (1 << 5);
    let flag_limit_in_pages: bool = flags & (1 << 4) != 0;
    //  let flag_read_exec_only: bool = flags & (1 << 3);
    let flag_seg32bit: bool = flags & 1 != 0;
    let mut flag: u8 = 0;
    if flag_limit_in_pages {
        flag |= gdt::F_GRANULARITY_PAGE;
    } else {
        flag |= gdt::F_GRANULARITY_BYTE;
    }
    if flag_seg32bit {
        flag |= gdt::F_SZ_32;
    }
    let base = data.get("base_addr")?.to_u64()?;
    //    let access = 0xf2;
    let access = gdt::A_PRESENT | gdt::A_PRIV_3 | gdt::A_WRITE | gdt::A_GROW_UP | gdt::A_DATA
        | gdt::A_UNKNOWN;
    // TODO better store number
    p.thread_areas.insert(num);
    gdt_write(cpu, p, num as u64, base, limit, flag, access)?;
    data.get_mut("entry_number")?.set_i64(num)?;
    let new_entry = data.pack(p.bits, p.endian)?;
    cpu.mem_write(addr, new_entry.as_slice())?;
    cpu.reg_write(RegisterX86::GS, 0x3 | (num * 8) as u64)?;
    Ok(0)
}

fn sys_read(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let fd = args[0];
    let buff = args[1];
    let count = args[2] as usize;
    println!{"sys_read({}, 0x{:x}, {})", fd, buff, count};
    match p.fs.read(fd, count) {
        Ok(data) => {
            cpu.mem_write(buff, data.as_slice())?;
            Ok(data.len() as u64)
        }
        Err(_) => Ok(MINUS_ONE),
    }
}

fn sys_getcwd(_: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let buff = args[0];
    let size = args[1] as usize;
    if buff == 0 {
        bail!{"buff == 0 not implemented"};
    }
    println!{"sys_getcwd(0x{:x}, {})", buff, size};
    write_string(cpu, buff, "/home/tdb/")?;
    Ok(buff)
}

fn sys_stat(p: &mut Process, cpu: &mut Cpu, args: [u64; 6]) -> Result<u64> {
    let mut buf = Vec::new();
    let file = read_string(cpu, args[0], &mut buf)?;
    let addr = args[1];
    println!{"sys_stat({}, {})", file, addr};
    match p.fs.get(file) {
        Some(f) => {
            let stat = f.stat.to_stat();
            let data = stat.pack(p.bits, Endian::LITTLE)?;
            cpu.mem_write(addr, data.as_slice())?;
            Ok(0)
        }
        None => Ok(MINUS_ONE),
    }
}

pub trait Kernel {
    fn init(&mut self, p: &mut Process, cpu: &mut Cpu) -> Result<()>;
    fn handle_syscall(&self, process: &mut Process, cpu: &mut Cpu) -> Result<String>;
    fn collect_registers(&self, cpu: &Cpu) -> Result<collections::HashMap<String, u64>>;
}

pub struct X86Kernel {
    pub syscalls: collections::HashMap<u64, Syscall>,
}

impl X86Kernel {
    fn load_default_syscalls(&mut self) {
        self.syscalls.insert(
            3,
            Syscall {
                name: String::from("sys_read"),
                syscall: Box::new(sys_read),
            },
        );
        self.syscalls.insert(
            4,
            Syscall {
                name: String::from("sys_write"),
                syscall: Box::new(sys_write),
            },
        );
        self.syscalls.insert(
            5,
            Syscall {
                name: String::from("sys_open"),
                syscall: Box::new(sys_open),
            },
        );
        self.syscalls.insert(
            6,
            Syscall {
                name: String::from("sys_close"),
                syscall: Box::new(sys_close),
            },
        );
        self.syscalls.insert(
            13,
            Syscall {
                name: String::from("sys_time"),
                syscall: Box::new(sys_time),
            },
        );
        self.syscalls.insert(
            33,
            Syscall {
                name: String::from("sys_access"),
                syscall: Box::new(sys_access),
            },
        );
        self.syscalls.insert(
            45,
            Syscall {
                name: String::from("sys_brk"),
                syscall: Box::new(sys_brk),
            },
        );
        self.syscalls.insert(
            85,
            Syscall {
                name: String::from("sys_readlink"),
                syscall: Box::new(sys_readlink),
            },
        );
        self.syscalls.insert(
            122,
            Syscall {
                name: String::from("sys_uname"),
                syscall: Box::new(sys_uname),
            },
        );
        self.syscalls.insert(
            125,
            Syscall {
                name: String::from("sys_mprotect"),
                syscall: Box::new(sys_mprotect),
            },
        );
        self.syscalls.insert(
            146,
            Syscall {
                name: String::from("sys_writev"),
                syscall: Box::new(sys_writev),
            },
        );
        self.syscalls.insert(
            183,
            Syscall {
                name: String::from("sys_getcwd"),
                syscall: Box::new(sys_getcwd),
            },
        );
        self.syscalls.insert(
            192,
            Syscall {
                name: String::from("sys_mmap2"),
                syscall: Box::new(sys_mmap2),
            },
        );
        self.syscalls.insert(
            197,
            Syscall {
                name: String::from("sys_fstat"),
                syscall: Box::new(sys_fstat),
            },
        );
        self.syscalls.insert(
            243,
            Syscall {
                name: String::from("sys_set_thread_area"),
                syscall: Box::new(sys_set_thread_area),
            },
        );
        self.syscalls.insert(
            252,
            Syscall {
                name: String::from("sys_exit_group"),
                syscall: Box::new(sys_exit_group),
            },
        );
    }
}

impl Kernel for X86Kernel {
    // http://asm.sourceforge.net/syscall.html
    // https://github.com/torvalds/linux/blob/204f144c9fcac355843412b6ba1150086488a208/arch/x86/entry/syscalls/syscall_32.tbl
    fn handle_syscall(&self, process: &mut Process, cpu: &mut Cpu) -> Result<String> {
        let syscall = cpu.reg_read(RegisterX86::EAX)?;
        let mut args = [0; 6];
        for (i, reg) in [
            RegisterX86::EBX,
            RegisterX86::ECX,
            RegisterX86::EDX,
            RegisterX86::ESI,
            RegisterX86::EDI,
        ].into_iter()
            .enumerate()
        {
            args[i] = cpu.reg_read(*reg)?;
        }
        println!("Syscall {} args {:?}", syscall, args);
        match self.syscalls.get(&syscall) {
            Some(ref sys) => {
                let cb = &sys.syscall;
                let res = cb(process, cpu, args)?;
                cpu.reg_write(RegisterX86::EAX, res)?;
                Ok(sys.name.clone())
            }
            None => bail!{"Invalid syscall: {} args {:?}", syscall, args},
        }
    }

    fn init(&mut self, p: &mut Process, cpu: &mut Cpu) -> Result<()> {
        let addr = p.allocate_memory(
            cpu,
            None,
            0x1000,
            unicorn::PROT_READ | unicorn::PROT_WRITE,
            "gdt",
        )?;
        p.gdt = Some(addr);
        let mmr = unicorn::uc_x86_mmr {
            selector: 0,
            base: addr,
            limit: 0x1000 - 1,
            flags: 0,
        };
        cpu.x86_mmr_write(X86MMR::GDTR, &mmr)?;
        let flags: u8 = gdt::F_SZ_32;
        let access: u8 =
            gdt::A_PRESENT | gdt::A_PRIV_0 | gdt::A_WRITE | gdt::A_GROW_DOWN | gdt::A_DATA;
        gdt_write(cpu, p, 1, 0, 0xffffff00, flags, access)?; // code
        gdt_write(cpu, p, 2, 0, 0xfffff000, flags, access)?; // data
        gdt_write(cpu, p, 3, 0, 0xfffff000, flags, access)?; // stack
        cpu.reg_write(RegisterX86::CS, (1 << 3) | gdt::S_PRIV_0 as u64)?;
        cpu.reg_write(RegisterX86::DS, (2 << 3) | gdt::S_PRIV_0 as u64)?;
        cpu.reg_write(RegisterX86::SS, (3 << 3) | gdt::S_PRIV_0 as u64)?;
        self.load_default_syscalls();
        Ok(())
    }

    fn collect_registers(&self, cpu: &Cpu) -> Result<collections::HashMap<String, u64>> {
        let regs: &[RegisterX86] = &[
            RegisterX86::EAX,
            RegisterX86::EBX,
            RegisterX86::ECX,
            RegisterX86::ESP,
            RegisterX86::EBP,
            RegisterX86::ESI,
            RegisterX86::EDI,
        ];
        let mut res = collections::HashMap::new();
        for r in regs {
            let r: RegisterX86 = *r;
            res.insert(format!("{:?}", r), cpu.reg_read(r)?);
        }
        return Ok(res);
    }
}

pub struct Amd64Kernel {
    pub syscalls: collections::HashMap<u64, Syscall>,
}

impl Amd64Kernel {
    fn load_default_syscalls(&mut self) {
        self.syscalls.insert(
            0,
            Syscall {
                name: String::from("sys_read"),
                syscall: Box::new(sys_read),
            },
        );
        self.syscalls.insert(
            1,
            Syscall {
                name: String::from("sys_write"),
                syscall: Box::new(sys_write),
            },
        );
        self.syscalls.insert(
            2,
            Syscall {
                name: String::from("sys_open"),
                syscall: Box::new(sys_open),
            },
        );
        self.syscalls.insert(
            3,
            Syscall {
                name: String::from("sys_close"),
                syscall: Box::new(sys_close),
            },
        );
        self.syscalls.insert(
            4,
            Syscall {
                name: String::from("sys_stat"),
                syscall: Box::new(sys_stat),
            },
        );
        self.syscalls.insert(
            5,
            Syscall {
                name: String::from("sys_fstat"),
                syscall: Box::new(sys_fstat),
            },
        );
        self.syscalls.insert(
            9,
            Syscall {
                name: String::from("sys_mmap2"),
                syscall: Box::new(sys_mmap2),
            },
        );
        self.syscalls.insert(
            10,
            Syscall {
                name: String::from("sys_mprotect"),
                syscall: Box::new(sys_mprotect),
            },
        );
        self.syscalls.insert(
            12,
            Syscall {
                name: String::from("sys_brk"),
                syscall: Box::new(sys_brk),
            },
        );
        self.syscalls.insert(
            20,
            Syscall {
                name: String::from("sys_writev"),
                syscall: Box::new(sys_writev),
            },
        );
        self.syscalls.insert(
            21,
            Syscall {
                name: String::from("sys_access"),
                syscall: Box::new(sys_access),
            },
        );
        self.syscalls.insert(
            63,
            Syscall {
                name: String::from("sys_uname"),
                syscall: Box::new(sys_uname),
            },
        );
        self.syscalls.insert(
            79,
            Syscall {
                name: String::from("sys_getcwd"),
                syscall: Box::new(sys_getcwd),
            },
        );
        self.syscalls.insert(
            89,
            Syscall {
                name: String::from("sys_readlink"),
                syscall: Box::new(sys_readlink),
            },
        );
        self.syscalls.insert(
            158,
            Syscall {
                name: String::from("sys_arch_prctl"),
                syscall: Box::new(sys_arch_prctl),
            },
        );
        self.syscalls.insert(
            231,
            Syscall {
                name: String::from("sys_exit_group"),
                syscall: Box::new(sys_exit_group),
            },
        );
    }
}

impl Kernel for Amd64Kernel {
    fn collect_registers(&self, cpu: &Cpu) -> Result<collections::HashMap<String, u64>> {
        let regs: &[RegisterX86] = &[
            RegisterX86::RAX,
            RegisterX86::RBX,
            RegisterX86::RCX,
            RegisterX86::RSP,
            RegisterX86::RBP,
            RegisterX86::RSI,
            RegisterX86::RDI,
        ];
        let mut res = collections::HashMap::new();
        for r in regs {
            let r: RegisterX86 = *r;
            res.insert(format!("{:?}", r), cpu.reg_read(r)?);
        }
        return Ok(res);
    }

    fn init(&mut self, _p: &mut Process, _cpu: &mut Cpu) -> Result<()> {
        self.load_default_syscalls();
        Ok(())
    }

    // http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
    // https://github.com/torvalds/linux/blob/7c8c03bfc7b9f5211d8a69eab7fee99c9fb4f449/tools/perf/arch/x86/entry/syscalls/syscall_64.tbl
    fn handle_syscall(&self, process: &mut Process, cpu: &mut Cpu) -> Result<String> {
        let syscall = cpu.reg_read(RegisterX86::RAX)?;
        let mut args = [0; 6];
        for (i, reg) in [
            RegisterX86::RDI,
            RegisterX86::RSI,
            RegisterX86::RDX,
            RegisterX86::R10,
            RegisterX86::R8,
            RegisterX86::R9,
        ].into_iter()
            .enumerate()
        {
            args[i] = cpu.reg_read(*reg)?;
        }
        match self.syscalls.get(&syscall) {
            Some(ref sys) => {
                let cb = &sys.syscall;
                let res = cb(process, cpu, args)?;
                cpu.reg_write(RegisterX86::RAX, res)?;
                Ok(sys.name.clone())
            }
            None => bail!{"Invalid syscall: {} args {:?}", syscall, args},
        }
    }
}
