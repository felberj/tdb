
#ifndef cheddar_generated_binary_h
#define cheddar_generated_binary_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>



typedef enum Bits {
	Bits_Bits32,
	Bits_Bits64,
} Bits;

typedef enum Endianness {
	Endianness_Big,
	Endianness_Little,
} Endianness;

typedef enum Machine {
	Machine_X86 = 3,
	Machine_X86_64 = 62,
} Machine;

typedef struct ElfIdent {
	Bits bits;
	Endianness endianness;
} ElfIdent;

typedef enum ElfType {
	ElfType_None = 0,
	ElfType_Rel = 1,
	ElfType_Exec = 2,
	ElfType_Dyn = 3,
	ElfType_Core = 4,
} ElfType;

typedef enum SectionType {
	SectionType_Rela = 4,
	SectionType_Rel = 9,
} SectionType;

typedef struct Section {
	SectionType s_type;
	uint64_t offset;
	uintptr_t size;
	uintptr_t entsize;
} Section;

typedef struct Ehdr {
	ElfIdent e_ident;
	ElfType e_type;
	uint64_t e_entry;
	uint64_t e_phoff;
	uint64_t e_phentsize;
	uint64_t e_phnum;
	uintptr_t e_shoff;
	uintptr_t e_shnum;
	uintptr_t e_shentsize;
	Machine machine;
} Ehdr;

typedef struct Elf {
	ElfIdent ident;
	Ehdr ehdr;
	Vec phdrs;
	Vec sections;
	Vec data;
} Elf;

Elf* libelf_parse_elf(char const* elf_path, int* ok);



#ifdef __cplusplus
}
#endif


#endif
