// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq,Clone,Default)]
pub struct CreateBinaryRequest {
    // message fields
    pub filename: ::std::string::String,
    pub data: ::std::vec::Vec<u8>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for CreateBinaryRequest {}

impl CreateBinaryRequest {
    pub fn new() -> CreateBinaryRequest {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static CreateBinaryRequest {
        static mut instance: ::protobuf::lazy::Lazy<CreateBinaryRequest> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const CreateBinaryRequest,
        };
        unsafe {
            instance.get(CreateBinaryRequest::new)
        }
    }

    // string filename = 1;

    pub fn clear_filename(&mut self) {
        self.filename.clear();
    }

    // Param is passed by value, moved
    pub fn set_filename(&mut self, v: ::std::string::String) {
        self.filename = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_filename(&mut self) -> &mut ::std::string::String {
        &mut self.filename
    }

    // Take field
    pub fn take_filename(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.filename, ::std::string::String::new())
    }

    pub fn get_filename(&self) -> &str {
        &self.filename
    }

    fn get_filename_for_reflect(&self) -> &::std::string::String {
        &self.filename
    }

    fn mut_filename_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.filename
    }

    // bytes data = 2;

    pub fn clear_data(&mut self) {
        self.data.clear();
    }

    // Param is passed by value, moved
    pub fn set_data(&mut self, v: ::std::vec::Vec<u8>) {
        self.data = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_data(&mut self) -> &mut ::std::vec::Vec<u8> {
        &mut self.data
    }

    // Take field
    pub fn take_data(&mut self) -> ::std::vec::Vec<u8> {
        ::std::mem::replace(&mut self.data, ::std::vec::Vec::new())
    }

    pub fn get_data(&self) -> &[u8] {
        &self.data
    }

    fn get_data_for_reflect(&self) -> &::std::vec::Vec<u8> {
        &self.data
    }

    fn mut_data_for_reflect(&mut self) -> &mut ::std::vec::Vec<u8> {
        &mut self.data
    }
}

impl ::protobuf::Message for CreateBinaryRequest {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.filename)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_proto3_bytes_into(wire_type, is, &mut self.data)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.filename.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.filename);
        }
        if !self.data.is_empty() {
            my_size += ::protobuf::rt::bytes_size(2, &self.data);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.filename.is_empty() {
            os.write_string(1, &self.filename)?;
        }
        if !self.data.is_empty() {
            os.write_bytes(2, &self.data)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for CreateBinaryRequest {
    fn new() -> CreateBinaryRequest {
        CreateBinaryRequest::new()
    }

    fn descriptor_static(_: ::std::option::Option<CreateBinaryRequest>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "filename",
                    CreateBinaryRequest::get_filename_for_reflect,
                    CreateBinaryRequest::mut_filename_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeBytes>(
                    "data",
                    CreateBinaryRequest::get_data_for_reflect,
                    CreateBinaryRequest::mut_data_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<CreateBinaryRequest>(
                    "CreateBinaryRequest",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for CreateBinaryRequest {
    fn clear(&mut self) {
        self.clear_filename();
        self.clear_data();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for CreateBinaryRequest {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for CreateBinaryRequest {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Binary {
    // message fields
    pub id: ::std::string::String,
    pub endian: Endian,
    pub machine: Machine,
    pub field_type: ElfType,
    pub bits: Bits,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Binary {}

impl Binary {
    pub fn new() -> Binary {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Binary {
        static mut instance: ::protobuf::lazy::Lazy<Binary> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Binary,
        };
        unsafe {
            instance.get(Binary::new)
        }
    }

    // string id = 1;

    pub fn clear_id(&mut self) {
        self.id.clear();
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: ::std::string::String) {
        self.id = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_id(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // Take field
    pub fn take_id(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.id, ::std::string::String::new())
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    fn get_id_for_reflect(&self) -> &::std::string::String {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // .tdb.Endian endian = 2;

    pub fn clear_endian(&mut self) {
        self.endian = Endian::LITTLE;
    }

    // Param is passed by value, moved
    pub fn set_endian(&mut self, v: Endian) {
        self.endian = v;
    }

    pub fn get_endian(&self) -> Endian {
        self.endian
    }

    fn get_endian_for_reflect(&self) -> &Endian {
        &self.endian
    }

    fn mut_endian_for_reflect(&mut self) -> &mut Endian {
        &mut self.endian
    }

    // .tdb.Machine machine = 3;

    pub fn clear_machine(&mut self) {
        self.machine = Machine::X86;
    }

    // Param is passed by value, moved
    pub fn set_machine(&mut self, v: Machine) {
        self.machine = v;
    }

    pub fn get_machine(&self) -> Machine {
        self.machine
    }

    fn get_machine_for_reflect(&self) -> &Machine {
        &self.machine
    }

    fn mut_machine_for_reflect(&mut self) -> &mut Machine {
        &mut self.machine
    }

    // .tdb.ElfType type = 4;

    pub fn clear_field_type(&mut self) {
        self.field_type = ElfType::NONE;
    }

    // Param is passed by value, moved
    pub fn set_field_type(&mut self, v: ElfType) {
        self.field_type = v;
    }

    pub fn get_field_type(&self) -> ElfType {
        self.field_type
    }

    fn get_field_type_for_reflect(&self) -> &ElfType {
        &self.field_type
    }

    fn mut_field_type_for_reflect(&mut self) -> &mut ElfType {
        &mut self.field_type
    }

    // .tdb.Bits bits = 5;

    pub fn clear_bits(&mut self) {
        self.bits = Bits::BITS_32;
    }

    // Param is passed by value, moved
    pub fn set_bits(&mut self, v: Bits) {
        self.bits = v;
    }

    pub fn get_bits(&self) -> Bits {
        self.bits
    }

    fn get_bits_for_reflect(&self) -> &Bits {
        &self.bits
    }

    fn mut_bits_for_reflect(&mut self) -> &mut Bits {
        &mut self.bits
    }
}

impl ::protobuf::Message for Binary {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.id)?;
                },
                2 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.endian, 2, &mut self.unknown_fields)?
                },
                3 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.machine, 3, &mut self.unknown_fields)?
                },
                4 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.field_type, 4, &mut self.unknown_fields)?
                },
                5 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.bits, 5, &mut self.unknown_fields)?
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.id.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.id);
        }
        if self.endian != Endian::LITTLE {
            my_size += ::protobuf::rt::enum_size(2, self.endian);
        }
        if self.machine != Machine::X86 {
            my_size += ::protobuf::rt::enum_size(3, self.machine);
        }
        if self.field_type != ElfType::NONE {
            my_size += ::protobuf::rt::enum_size(4, self.field_type);
        }
        if self.bits != Bits::BITS_32 {
            my_size += ::protobuf::rt::enum_size(5, self.bits);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.id.is_empty() {
            os.write_string(1, &self.id)?;
        }
        if self.endian != Endian::LITTLE {
            os.write_enum(2, self.endian.value())?;
        }
        if self.machine != Machine::X86 {
            os.write_enum(3, self.machine.value())?;
        }
        if self.field_type != ElfType::NONE {
            os.write_enum(4, self.field_type.value())?;
        }
        if self.bits != Bits::BITS_32 {
            os.write_enum(5, self.bits.value())?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Binary {
    fn new() -> Binary {
        Binary::new()
    }

    fn descriptor_static(_: ::std::option::Option<Binary>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "id",
                    Binary::get_id_for_reflect,
                    Binary::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<Endian>>(
                    "endian",
                    Binary::get_endian_for_reflect,
                    Binary::mut_endian_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<Machine>>(
                    "machine",
                    Binary::get_machine_for_reflect,
                    Binary::mut_machine_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<ElfType>>(
                    "type",
                    Binary::get_field_type_for_reflect,
                    Binary::mut_field_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<Bits>>(
                    "bits",
                    Binary::get_bits_for_reflect,
                    Binary::mut_bits_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Binary>(
                    "Binary",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Binary {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_endian();
        self.clear_machine();
        self.clear_field_type();
        self.clear_bits();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Binary {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Binary {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum Bits {
    BITS_32 = 0,
    BITS_64 = 1,
}

impl ::protobuf::ProtobufEnum for Bits {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<Bits> {
        match value {
            0 => ::std::option::Option::Some(Bits::BITS_32),
            1 => ::std::option::Option::Some(Bits::BITS_64),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [Bits] = &[
            Bits::BITS_32,
            Bits::BITS_64,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<Bits>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("Bits", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for Bits {
}

impl ::std::default::Default for Bits {
    fn default() -> Self {
        Bits::BITS_32
    }
}

impl ::protobuf::reflect::ProtobufValue for Bits {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum Endian {
    LITTLE = 0,
    BIG = 1,
}

impl ::protobuf::ProtobufEnum for Endian {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<Endian> {
        match value {
            0 => ::std::option::Option::Some(Endian::LITTLE),
            1 => ::std::option::Option::Some(Endian::BIG),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [Endian] = &[
            Endian::LITTLE,
            Endian::BIG,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<Endian>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("Endian", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for Endian {
}

impl ::std::default::Default for Endian {
    fn default() -> Self {
        Endian::LITTLE
    }
}

impl ::protobuf::reflect::ProtobufValue for Endian {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum ElfType {
    NONE = 0,
    REL = 1,
    EXEC = 2,
    DYN = 3,
    CORE = 4,
}

impl ::protobuf::ProtobufEnum for ElfType {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<ElfType> {
        match value {
            0 => ::std::option::Option::Some(ElfType::NONE),
            1 => ::std::option::Option::Some(ElfType::REL),
            2 => ::std::option::Option::Some(ElfType::EXEC),
            3 => ::std::option::Option::Some(ElfType::DYN),
            4 => ::std::option::Option::Some(ElfType::CORE),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [ElfType] = &[
            ElfType::NONE,
            ElfType::REL,
            ElfType::EXEC,
            ElfType::DYN,
            ElfType::CORE,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<ElfType>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("ElfType", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for ElfType {
}

impl ::std::default::Default for ElfType {
    fn default() -> Self {
        ElfType::NONE
    }
}

impl ::protobuf::reflect::ProtobufValue for ElfType {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum Machine {
    X86 = 0,
    X86_64 = 1,
}

impl ::protobuf::ProtobufEnum for Machine {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<Machine> {
        match value {
            0 => ::std::option::Option::Some(Machine::X86),
            1 => ::std::option::Option::Some(Machine::X86_64),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [Machine] = &[
            Machine::X86,
            Machine::X86_64,
        ];
        values
    }

    fn enum_descriptor_static(_: ::std::option::Option<Machine>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("Machine", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for Machine {
}

impl ::std::default::Default for Machine {
    fn default() -> Self {
        Machine::X86
    }
}

impl ::protobuf::reflect::ProtobufValue for Machine {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x0cbinary.proto\x12\x03tdb\x1a\nfile.proto\"E\n\x13CreateBinaryReques\
    t\x12\x1a\n\x08filename\x18\x01\x20\x01(\tR\x08filename\x12\x12\n\x04dat\
    a\x18\x02\x20\x01(\x0cR\x04data\"\xa6\x01\n\x06Binary\x12\x0e\n\x02id\
    \x18\x01\x20\x01(\tR\x02id\x12#\n\x06endian\x18\x02\x20\x01(\x0e2\x0b.td\
    b.EndianR\x06endian\x12&\n\x07machine\x18\x03\x20\x01(\x0e2\x0c.tdb.Mach\
    ineR\x07machine\x12\x20\n\x04type\x18\x04\x20\x01(\x0e2\x0c.tdb.ElfTypeR\
    \x04type\x12\x1d\n\x04bits\x18\x05\x20\x01(\x0e2\t.tdb.BitsR\x04bits*\
    \x20\n\x04Bits\x12\x0b\n\x07BITS_32\x10\0\x12\x0b\n\x07BITS_64\x10\x01*\
    \x1d\n\x06Endian\x12\n\n\x06LITTLE\x10\0\x12\x07\n\x03BIG\x10\x01*9\n\
    \x07ElfType\x12\x08\n\x04NONE\x10\0\x12\x07\n\x03REL\x10\x01\x12\x08\n\
    \x04EXEC\x10\x02\x12\x07\n\x03DYN\x10\x03\x12\x08\n\x04CORE\x10\x04*\x1e\
    \n\x07Machine\x12\x07\n\x03X86\x10\0\x12\n\n\x06X86_64\x10\x012H\n\rBina\
    ryManager\x127\n\x0cCreateBinary\x12\x18.tdb.CreateBinaryRequest\x1a\x0b\
    .tdb.Binary\"\0b\x06proto3\
";

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
