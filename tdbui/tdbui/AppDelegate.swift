//
//  AppDelegate.swift
//  tdbui
//
//  Created by Jonas Felber on 20.01.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

