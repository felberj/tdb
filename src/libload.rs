use binary::{parse_elf, Bits, Elf, ElfType, Endian, SectionType, SegmentType};
use byteorder::{LittleEndian, WriteBytesExt};
use error::*;
use file;
use libtdb::Cpu;
use std;
use std::collections::{HashMap, HashSet};
use structs::Struct;
use unicorn;
use unicorn::Protection;

pub struct Process {
    pub brk: u64,          // break value
    pub internal_brk: u64, // used for mallocs in the kernel
    pub bits: Bits,
    pub endian: Endian,
    pub thread_areas: HashSet<i64>,
    pub gdt: Option<u64>,
    pub program_arguments: Vec<String>,
    pub stdout: String,
    pub fs: file::System,
    pub entry_point: Option<u64>,
    pub memory_names: HashMap<u64, String>,
}

impl Process {
    pub fn allocate_memory(
        &mut self,
        cpu: &mut Cpu,
        addr: Option<u64>,
        size: u64,
        protection: Protection,
        name: &str,
    ) -> Result<u64> {
        let mut size = size;
        let rem = size % 0x1000;
        if rem != 0 {
            size += 0x1000 - rem;
        }
        let addr = match addr {
            Some(a) => a,
            None => {
                let addr = self.internal_brk;
                self.internal_brk += size;
                addr
            }
        };
        if cpu.mem_regions()?.iter().any(|r| r.begin == addr) {
            bail!{"already allocated page at {}", addr}; // TODO correct?
        }
        if addr % 0x1000 != 0 {
            bail!{"addr {} must be page aligned", addr};
        }
        cpu.mem_map(addr, size as usize, protection)?;
        self.memory_names.insert(addr, String::from(name));
        Ok(addr)
    }
}

// Loads the elf into the memory. When the elf requieres a dynamic linker, the linker gets loaded first.
// https://github.com/torvalds/linux/blob/master/fs/binfmt_elf.c#L682
pub fn load_elf(process: &mut Process, cpu: &mut Cpu, elf: &Elf) -> Result<()> {
    //    let interpreter = try!(parse_elf_interp(&elf));
    let relocation_offset = get_relocation_offset(elf);
    let elf_location = try!(_load_elf(process, cpu, &elf, relocation_offset));

    let mut stack_prot = unicorn::PROT_READ | unicorn::PROT_WRITE;
    for phdr in &elf.phdrs {
        if phdr.p_type == SegmentType::GnuStack {
            if phdr.executable {
                stack_prot |= unicorn::PROT_EXEC;
            }
            break;
        }
    }
    // FIXME set personality https://github.com/torvalds/linux/blob/master/fs/binfmt_elf.c#L863

    let stack_addr = match elf.ident.bits {
        Bits::BITS_32 => 0xfff0e000,
        Bits::BITS_64 => 0x7fff6c845000,
    };
    process.allocate_memory(
        cpu,
        Some(stack_addr - 0x400000),
        0x400000, // 4 MB
        stack_prot,
        "Stack",
    )?;

    // Stack Layout http://www.win.tue.nl/~aeb/linux/hh/stack-layout.html
    let exec_name = "./my_executable";
    let env_vars = &["ENVVAR1=1", "ENVVAR1=2"];
    let rand: &[u8] = &[
        100, 252, 216, 147, 131, 110, 0, 9, 175, 17, 0, 153, 121, 121, 75, 107
    ];

    let mut stack_ptr = stack_addr;
    stack_ptr -= write_string_to_stack(cpu, stack_ptr, exec_name)? as u64;
    let at_execfn = stack_ptr;

    let mut env_pointers: Vec<u64> = Vec::with_capacity(env_vars.len());
    for env in env_vars.iter().rev() {
        stack_ptr -= write_string_to_stack(cpu, stack_ptr, env)? as u64;
        env_pointers.push(stack_ptr);
    }

    let mut arg_pointers: Vec<u64> = Vec::with_capacity(env_vars.len() + 1);
    arg_pointers.push(at_execfn);
    for arg in process.program_arguments.iter() {
        stack_ptr -= write_string_to_stack(cpu, stack_ptr, arg)? as u64;
        arg_pointers.push(stack_ptr);
    }

    // FIXME random padding?

    stack_ptr -= write_string_to_stack(
        cpu,
        stack_ptr,
        match elf.ident.bits {
            Bits::BITS_32 => "x86",
            Bits::BITS_64 => "x86_64",
        },
    )? as u64;
    let at_platform = stack_ptr;

    stack_ptr -= rand.len() as u64;
    try!(cpu.mem_write(stack_ptr, rand));
    let at_random = stack_ptr;

    stack_ptr &= 0xFFFFFFFFFFFFFF00;

    let _bits = elf.ident.bits;
    let mut res: Vec<u8> = Vec::new();
    {
        let mut write_word = |p: u64| {
            match _bits {
                Bits::BITS_64 => {
                    res.write_u64::<LittleEndian>(p).unwrap();
                }
                Bits::BITS_32 => {
                    res.write_u32::<LittleEndian>(p as u32).unwrap();
                }
            };
        };

        // the stackpointer will point here when _start is called
        write_word(arg_pointers.len() as u64);
        for i in arg_pointers.iter() {
            write_word(*i);
        }
        write_word(0); // nil at end of args

        for i in env_pointers.iter() {
            write_word(*i);
        }
        write_word(0); // nil at end of env

        let entry = elf.ehdr.e_entry + relocation_offset;
        process.entry_point = Some(entry);
        {
            let mut write_auxiliary_entry = |key: u64, val: u64| {
                write_word(key);
                write_word(val);
            };

            write_auxiliary_entry(15, at_platform);
            write_auxiliary_entry(31, at_execfn);
            write_auxiliary_entry(25, at_random);
            write_auxiliary_entry(23, 0); // AT_SECURE
            write_auxiliary_entry(14, 1000); // AT_EGID
            write_auxiliary_entry(13, 1000); // AT_GID
            write_auxiliary_entry(12, 1000); // AT_EUID
            write_auxiliary_entry(11, 1000); // AT_UID
            write_auxiliary_entry(9, entry); // AT_ENTRY(9)
            write_auxiliary_entry(8, 0); //AT_FLAGS(8)

            write_auxiliary_entry(5, elf.ehdr.e_phnum); // AT_PHNUM(5)
            write_auxiliary_entry(4, elf.ehdr.e_phentsize); // AT_PHENT(4)
            write_auxiliary_entry(3, elf.ehdr.e_phoff + elf_location); // AT_PHDR(3)
            write_auxiliary_entry(17, 100); // AT_CLKTCK(17)
            write_auxiliary_entry(6, 4096); // AT_PAGESZ(6)
            write_auxiliary_entry(16, 0); // AT_HWCAP(16) TODO
            write_auxiliary_entry(0, 0); // AT_NULL
        }
    }
    stack_ptr -= res.len() as u64;
    match _bits {
        Bits::BITS_64 => cpu.reg_write(unicorn::RegisterX86::RSP, stack_ptr)?,
        Bits::BITS_32 => cpu.reg_write(unicorn::RegisterX86::ESP, stack_ptr)?,
    };
    cpu.mem_write(stack_ptr as u64, res.as_slice())?;
    return Ok(());
}

fn get_relocation_offset(elf: &Elf) -> u64 {
    let offset = match elf.ident.bits {
        Bits::BITS_32 => 0x800000,
        Bits::BITS_64 => 0x8888888800000000,
    };
    match elf.ehdr.e_type {
        ElfType::REL => offset,
        ElfType::DYN => offset,
        _ => 0,
    }
}

// Writes the string to the address (the length of the data is substracted from the address)
// into the process and returns how many bytes where written.
fn write_string_to_stack(cpu: &mut Cpu, address: u64, string: &str) -> Result<usize> {
    let data = string.as_bytes();
    let mut vec: Vec<u8> = Vec::with_capacity(data.len() + 1);
    vec.extend_from_slice(data);
    vec.push(0); // terminating zero byte
    try!(cpu.mem_write(address - vec.len() as u64, vec.as_slice()));
    return Ok(vec.len());
}

// Actually loads the elf into memory. Returns position where the elf starts
// https://lwn.net/Articles/631631/
fn _load_elf(
    process: &mut Process,
    cpu: &mut Cpu,
    elf: &Elf,
    relocation_offset: u64,
) -> Result<u64> {
    let page_size = 0x1000;
    let mut binary_location = None;
    for p in &elf.phdrs {
        if p.p_type != SegmentType::Load {
            continue;
        }
        let mut prot = unicorn::PROT_NONE;
        if p.readable {
            prot |= unicorn::PROT_READ;
        }
        if p.writable {
            prot |= unicorn::PROT_WRITE;
        }
        if p.executable {
            prot |= unicorn::PROT_EXEC;
        }
        // mapping memory
        let mut normalised = (p.p_vaddr + relocation_offset) & !(page_size - 1);
        while normalised < p.p_vaddr + p.p_memsz + relocation_offset {
            process.allocate_memory(cpu, Some(normalised), page_size, prot, &elf.name)?;
            normalised += page_size;
        }
        // load the data into memory
        let data = elf.data
            .as_slice()
            .get((p.p_offset as usize)..((p.p_offset + p.p_filesz) as usize));
        if data.is_none() {
            bail!("Unable to load data of header");
        }
        try!(cpu.mem_write(p.p_vaddr + relocation_offset, data.unwrap()));
        if p.p_offset == 0 {
            binary_location = Some(p.p_vaddr + relocation_offset);
        }
    }
    if binary_location.is_none() {
        bail!{"no binary location"};
    }
    let binary_location = binary_location.unwrap();
    // iterate over realocation entries and realocate them
    for s in &elf.sections {
        match s.s_type {
            SectionType::Rel => {}
            SectionType::Rela => {
                // Iterate over entries
                let mut fields = Vec::new();
                fields.push(Struct::Uword("offset", 0));
                fields.push(Struct::Uword("type", 0));
                fields.push(Struct::Iword("addend", 0));
                let mut entry = Struct::Struct("", fields);
                for i in 0..(s.size / s.entsize) {
                    let ptr = binary_location + s.offset + (s.entsize * i) as u64;
                    let data = cpu.mem_read(ptr, entry.size(process.bits))?;
                    entry.unpack(process.bits, process.endian, data.as_slice())?;
                    let offset = entry.get("offset")?.to_u64()?;
                    let typ = entry.get("type")?.to_u64()?;
                    let addend = entry.get("addend")?.to_i64()?;
                    match process.bits {
                        Bits::BITS_64 => {
                            match typ & 0xff {
                                6 => {
                                    // R_X86_64_GLOB_DAT
                                    let mut word = Struct::Uword("", 0);
                                    let data = cpu.mem_read(offset + binary_location, 8)?;
                                    word.unpack(process.bits, process.endian, data.as_slice())?;
                                    // TODO I think I need to get the symbol value in a different way
                                    let val = if addend < 0 {
                                        word.to_u64()? - (addend as u64)
                                    } else {
                                        word.to_u64()? + (addend as u64)
                                    };
                                    word.set_u64(val)?;
                                    cpu.mem_write(
                                        offset as u64 + binary_location,
                                        word.pack(process.bits, process.endian)?.as_slice(),
                                    )?;
                                }
                                8 => {
                                    // R_X86_64_RELATIVE
                                    let mut word = Struct::Uword("", 0);

                                    let val = if addend < 0 {
                                        // FIXME correct?!
                                        relocation_offset - (addend as u64)
                                    } else {
                                        relocation_offset + (addend as u64)
                                    };
                                    word.set_u64(val)?;
                                    cpu.mem_write(
                                        offset as u64 + binary_location,
                                        word.pack(process.bits, process.endian)?.as_slice(),
                                    )?;
                                }
                                7 => {}  // R_X86_64_JUMP_SLOT
                                37 => {} // R_X86_64_IRELATIVE TODO do not ignore it
                                _ => bail!{"x86_64 realication type {} not implemented", typ},
                            }
                        }
                        Bits::BITS_32 => {
                            match typ & 0xff {
                                7 => {} // R_X86_64_JUMP_SLOT
                                _ => bail!{"x86 realication type {} not implemented", typ},
                            }
                        }
                    }
                }
            }
        }
    }
    Ok(binary_location)
}

// Looks through the elf headers for a linker and tries to load it by passing the path to the linker to the file_loader.
pub fn parse_elf_interp(
    elf: &Elf,
    file_loader: &Fn(&str) -> Result<Vec<u8>>,
) -> Result<Option<Elf>> {
    for phdr in &elf.phdrs {
        if phdr.p_type != SegmentType::Interp {
            continue;
        }
        // This is the program interpreter used for
        // shared libraries - for now assume that this
        // is an a.out format binary
        // FIXME figure out real MAX_PATH
        if phdr.p_filesz > 255 || phdr.p_filesz < 2 {
            bail!("Invalid filesize");
        }
        let path_data = elf.data
            .get((phdr.p_offset as usize)..((phdr.p_offset + phdr.p_filesz) as usize));
        if path_data.is_none() {
            bail!("Invalid interpreter path data");
        }
        let path_str = try!(
            std::ffi::CStr::from_bytes_with_nul(path_data.unwrap()).or(Err("from_bytes_with_nul"))
        );
        let interpreter_path = try!(
            path_str
                .to_str()
                .or(Err("UTF8 error while getting the interpreter."))
        );
        // FIXME how to express this more pretty?
        let data = try!(file_loader(interpreter_path.into()));
        let res = try!(parse_elf(data));
        return Ok(Some(res));
    }
    return Ok(None);
}
