// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq,Clone,Default)]
pub struct GetSessionRequest {
    // message fields
    pub id: ::std::string::String,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for GetSessionRequest {}

impl GetSessionRequest {
    pub fn new() -> GetSessionRequest {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static GetSessionRequest {
        static mut instance: ::protobuf::lazy::Lazy<GetSessionRequest> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const GetSessionRequest,
        };
        unsafe {
            instance.get(GetSessionRequest::new)
        }
    }

    // string id = 1;

    pub fn clear_id(&mut self) {
        self.id.clear();
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: ::std::string::String) {
        self.id = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_id(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // Take field
    pub fn take_id(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.id, ::std::string::String::new())
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    fn get_id_for_reflect(&self) -> &::std::string::String {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }
}

impl ::protobuf::Message for GetSessionRequest {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.id)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.id.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.id);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.id.is_empty() {
            os.write_string(1, &self.id)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for GetSessionRequest {
    fn new() -> GetSessionRequest {
        GetSessionRequest::new()
    }

    fn descriptor_static(_: ::std::option::Option<GetSessionRequest>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "id",
                    GetSessionRequest::get_id_for_reflect,
                    GetSessionRequest::mut_id_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<GetSessionRequest>(
                    "GetSessionRequest",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for GetSessionRequest {
    fn clear(&mut self) {
        self.clear_id();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for GetSessionRequest {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for GetSessionRequest {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct CreateSessionRequest {
    // message fields
    pub session: ::protobuf::SingularPtrField<Session>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for CreateSessionRequest {}

impl CreateSessionRequest {
    pub fn new() -> CreateSessionRequest {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static CreateSessionRequest {
        static mut instance: ::protobuf::lazy::Lazy<CreateSessionRequest> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const CreateSessionRequest,
        };
        unsafe {
            instance.get(CreateSessionRequest::new)
        }
    }

    // .tdb.Session session = 1;

    pub fn clear_session(&mut self) {
        self.session.clear();
    }

    pub fn has_session(&self) -> bool {
        self.session.is_some()
    }

    // Param is passed by value, moved
    pub fn set_session(&mut self, v: Session) {
        self.session = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_session(&mut self) -> &mut Session {
        if self.session.is_none() {
            self.session.set_default();
        }
        self.session.as_mut().unwrap()
    }

    // Take field
    pub fn take_session(&mut self) -> Session {
        self.session.take().unwrap_or_else(|| Session::new())
    }

    pub fn get_session(&self) -> &Session {
        self.session.as_ref().unwrap_or_else(|| Session::default_instance())
    }

    fn get_session_for_reflect(&self) -> &::protobuf::SingularPtrField<Session> {
        &self.session
    }

    fn mut_session_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<Session> {
        &mut self.session
    }
}

impl ::protobuf::Message for CreateSessionRequest {
    fn is_initialized(&self) -> bool {
        for v in &self.session {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.session)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(ref v) = self.session.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(ref v) = self.session.as_ref() {
            os.write_tag(1, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for CreateSessionRequest {
    fn new() -> CreateSessionRequest {
        CreateSessionRequest::new()
    }

    fn descriptor_static(_: ::std::option::Option<CreateSessionRequest>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Session>>(
                    "session",
                    CreateSessionRequest::get_session_for_reflect,
                    CreateSessionRequest::mut_session_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<CreateSessionRequest>(
                    "CreateSessionRequest",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for CreateSessionRequest {
    fn clear(&mut self) {
        self.clear_session();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for CreateSessionRequest {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for CreateSessionRequest {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct SessionConfig {
    // message fields
    pub brk: u64,
    pub internal_brk: u64,
    pub bits: super::binary::Bits,
    pub endian: super::binary::Endian,
    pub program_arguments: ::protobuf::RepeatedField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for SessionConfig {}

impl SessionConfig {
    pub fn new() -> SessionConfig {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static SessionConfig {
        static mut instance: ::protobuf::lazy::Lazy<SessionConfig> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const SessionConfig,
        };
        unsafe {
            instance.get(SessionConfig::new)
        }
    }

    // uint64 brk = 1;

    pub fn clear_brk(&mut self) {
        self.brk = 0;
    }

    // Param is passed by value, moved
    pub fn set_brk(&mut self, v: u64) {
        self.brk = v;
    }

    pub fn get_brk(&self) -> u64 {
        self.brk
    }

    fn get_brk_for_reflect(&self) -> &u64 {
        &self.brk
    }

    fn mut_brk_for_reflect(&mut self) -> &mut u64 {
        &mut self.brk
    }

    // uint64 internal_brk = 2;

    pub fn clear_internal_brk(&mut self) {
        self.internal_brk = 0;
    }

    // Param is passed by value, moved
    pub fn set_internal_brk(&mut self, v: u64) {
        self.internal_brk = v;
    }

    pub fn get_internal_brk(&self) -> u64 {
        self.internal_brk
    }

    fn get_internal_brk_for_reflect(&self) -> &u64 {
        &self.internal_brk
    }

    fn mut_internal_brk_for_reflect(&mut self) -> &mut u64 {
        &mut self.internal_brk
    }

    // .tdb.Bits bits = 3;

    pub fn clear_bits(&mut self) {
        self.bits = super::binary::Bits::BITS_32;
    }

    // Param is passed by value, moved
    pub fn set_bits(&mut self, v: super::binary::Bits) {
        self.bits = v;
    }

    pub fn get_bits(&self) -> super::binary::Bits {
        self.bits
    }

    fn get_bits_for_reflect(&self) -> &super::binary::Bits {
        &self.bits
    }

    fn mut_bits_for_reflect(&mut self) -> &mut super::binary::Bits {
        &mut self.bits
    }

    // .tdb.Endian endian = 4;

    pub fn clear_endian(&mut self) {
        self.endian = super::binary::Endian::LITTLE;
    }

    // Param is passed by value, moved
    pub fn set_endian(&mut self, v: super::binary::Endian) {
        self.endian = v;
    }

    pub fn get_endian(&self) -> super::binary::Endian {
        self.endian
    }

    fn get_endian_for_reflect(&self) -> &super::binary::Endian {
        &self.endian
    }

    fn mut_endian_for_reflect(&mut self) -> &mut super::binary::Endian {
        &mut self.endian
    }

    // repeated string program_arguments = 5;

    pub fn clear_program_arguments(&mut self) {
        self.program_arguments.clear();
    }

    // Param is passed by value, moved
    pub fn set_program_arguments(&mut self, v: ::protobuf::RepeatedField<::std::string::String>) {
        self.program_arguments = v;
    }

    // Mutable pointer to the field.
    pub fn mut_program_arguments(&mut self) -> &mut ::protobuf::RepeatedField<::std::string::String> {
        &mut self.program_arguments
    }

    // Take field
    pub fn take_program_arguments(&mut self) -> ::protobuf::RepeatedField<::std::string::String> {
        ::std::mem::replace(&mut self.program_arguments, ::protobuf::RepeatedField::new())
    }

    pub fn get_program_arguments(&self) -> &[::std::string::String] {
        &self.program_arguments
    }

    fn get_program_arguments_for_reflect(&self) -> &::protobuf::RepeatedField<::std::string::String> {
        &self.program_arguments
    }

    fn mut_program_arguments_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<::std::string::String> {
        &mut self.program_arguments
    }
}

impl ::protobuf::Message for SessionConfig {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.brk = tmp;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.internal_brk = tmp;
                },
                3 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.bits, 3, &mut self.unknown_fields)?
                },
                4 => {
                    ::protobuf::rt::read_proto3_enum_with_unknown_fields_into(wire_type, is, &mut self.endian, 4, &mut self.unknown_fields)?
                },
                5 => {
                    ::protobuf::rt::read_repeated_string_into(wire_type, is, &mut self.program_arguments)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if self.brk != 0 {
            my_size += ::protobuf::rt::value_size(1, self.brk, ::protobuf::wire_format::WireTypeVarint);
        }
        if self.internal_brk != 0 {
            my_size += ::protobuf::rt::value_size(2, self.internal_brk, ::protobuf::wire_format::WireTypeVarint);
        }
        if self.bits != super::binary::Bits::BITS_32 {
            my_size += ::protobuf::rt::enum_size(3, self.bits);
        }
        if self.endian != super::binary::Endian::LITTLE {
            my_size += ::protobuf::rt::enum_size(4, self.endian);
        }
        for value in &self.program_arguments {
            my_size += ::protobuf::rt::string_size(5, &value);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if self.brk != 0 {
            os.write_uint64(1, self.brk)?;
        }
        if self.internal_brk != 0 {
            os.write_uint64(2, self.internal_brk)?;
        }
        if self.bits != super::binary::Bits::BITS_32 {
            os.write_enum(3, self.bits.value())?;
        }
        if self.endian != super::binary::Endian::LITTLE {
            os.write_enum(4, self.endian.value())?;
        }
        for v in &self.program_arguments {
            os.write_string(5, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for SessionConfig {
    fn new() -> SessionConfig {
        SessionConfig::new()
    }

    fn descriptor_static(_: ::std::option::Option<SessionConfig>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "brk",
                    SessionConfig::get_brk_for_reflect,
                    SessionConfig::mut_brk_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "internal_brk",
                    SessionConfig::get_internal_brk_for_reflect,
                    SessionConfig::mut_internal_brk_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<super::binary::Bits>>(
                    "bits",
                    SessionConfig::get_bits_for_reflect,
                    SessionConfig::mut_bits_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeEnum<super::binary::Endian>>(
                    "endian",
                    SessionConfig::get_endian_for_reflect,
                    SessionConfig::mut_endian_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "program_arguments",
                    SessionConfig::get_program_arguments_for_reflect,
                    SessionConfig::mut_program_arguments_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<SessionConfig>(
                    "SessionConfig",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for SessionConfig {
    fn clear(&mut self) {
        self.clear_brk();
        self.clear_internal_brk();
        self.clear_bits();
        self.clear_endian();
        self.clear_program_arguments();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for SessionConfig {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for SessionConfig {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Session {
    // message fields
    pub id: ::std::string::String,
    pub config: ::protobuf::SingularPtrField<SessionConfig>,
    pub steps_count: u64,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Session {}

impl Session {
    pub fn new() -> Session {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Session {
        static mut instance: ::protobuf::lazy::Lazy<Session> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Session,
        };
        unsafe {
            instance.get(Session::new)
        }
    }

    // string id = 1;

    pub fn clear_id(&mut self) {
        self.id.clear();
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: ::std::string::String) {
        self.id = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_id(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // Take field
    pub fn take_id(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.id, ::std::string::String::new())
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    fn get_id_for_reflect(&self) -> &::std::string::String {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // .tdb.SessionConfig config = 2;

    pub fn clear_config(&mut self) {
        self.config.clear();
    }

    pub fn has_config(&self) -> bool {
        self.config.is_some()
    }

    // Param is passed by value, moved
    pub fn set_config(&mut self, v: SessionConfig) {
        self.config = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_config(&mut self) -> &mut SessionConfig {
        if self.config.is_none() {
            self.config.set_default();
        }
        self.config.as_mut().unwrap()
    }

    // Take field
    pub fn take_config(&mut self) -> SessionConfig {
        self.config.take().unwrap_or_else(|| SessionConfig::new())
    }

    pub fn get_config(&self) -> &SessionConfig {
        self.config.as_ref().unwrap_or_else(|| SessionConfig::default_instance())
    }

    fn get_config_for_reflect(&self) -> &::protobuf::SingularPtrField<SessionConfig> {
        &self.config
    }

    fn mut_config_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<SessionConfig> {
        &mut self.config
    }

    // uint64 steps_count = 3;

    pub fn clear_steps_count(&mut self) {
        self.steps_count = 0;
    }

    // Param is passed by value, moved
    pub fn set_steps_count(&mut self, v: u64) {
        self.steps_count = v;
    }

    pub fn get_steps_count(&self) -> u64 {
        self.steps_count
    }

    fn get_steps_count_for_reflect(&self) -> &u64 {
        &self.steps_count
    }

    fn mut_steps_count_for_reflect(&mut self) -> &mut u64 {
        &mut self.steps_count
    }
}

impl ::protobuf::Message for Session {
    fn is_initialized(&self) -> bool {
        for v in &self.config {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.id)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.config)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.steps_count = tmp;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.id.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.id);
        }
        if let Some(ref v) = self.config.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        if self.steps_count != 0 {
            my_size += ::protobuf::rt::value_size(3, self.steps_count, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.id.is_empty() {
            os.write_string(1, &self.id)?;
        }
        if let Some(ref v) = self.config.as_ref() {
            os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        if self.steps_count != 0 {
            os.write_uint64(3, self.steps_count)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Session {
    fn new() -> Session {
        Session::new()
    }

    fn descriptor_static(_: ::std::option::Option<Session>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "id",
                    Session::get_id_for_reflect,
                    Session::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<SessionConfig>>(
                    "config",
                    Session::get_config_for_reflect,
                    Session::mut_config_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "steps_count",
                    Session::get_steps_count_for_reflect,
                    Session::mut_steps_count_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Session>(
                    "Session",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Session {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_config();
        self.clear_steps_count();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Session {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Session {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct GetSessionStateRequest {
    // message fields
    pub session_id: ::std::string::String,
    pub step_index: u64,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for GetSessionStateRequest {}

impl GetSessionStateRequest {
    pub fn new() -> GetSessionStateRequest {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static GetSessionStateRequest {
        static mut instance: ::protobuf::lazy::Lazy<GetSessionStateRequest> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const GetSessionStateRequest,
        };
        unsafe {
            instance.get(GetSessionStateRequest::new)
        }
    }

    // string session_id = 1;

    pub fn clear_session_id(&mut self) {
        self.session_id.clear();
    }

    // Param is passed by value, moved
    pub fn set_session_id(&mut self, v: ::std::string::String) {
        self.session_id = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_session_id(&mut self) -> &mut ::std::string::String {
        &mut self.session_id
    }

    // Take field
    pub fn take_session_id(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.session_id, ::std::string::String::new())
    }

    pub fn get_session_id(&self) -> &str {
        &self.session_id
    }

    fn get_session_id_for_reflect(&self) -> &::std::string::String {
        &self.session_id
    }

    fn mut_session_id_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.session_id
    }

    // uint64 step_index = 2;

    pub fn clear_step_index(&mut self) {
        self.step_index = 0;
    }

    // Param is passed by value, moved
    pub fn set_step_index(&mut self, v: u64) {
        self.step_index = v;
    }

    pub fn get_step_index(&self) -> u64 {
        self.step_index
    }

    fn get_step_index_for_reflect(&self) -> &u64 {
        &self.step_index
    }

    fn mut_step_index_for_reflect(&mut self) -> &mut u64 {
        &mut self.step_index
    }
}

impl ::protobuf::Message for GetSessionStateRequest {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.session_id)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.step_index = tmp;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.session_id.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.session_id);
        }
        if self.step_index != 0 {
            my_size += ::protobuf::rt::value_size(2, self.step_index, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.session_id.is_empty() {
            os.write_string(1, &self.session_id)?;
        }
        if self.step_index != 0 {
            os.write_uint64(2, self.step_index)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for GetSessionStateRequest {
    fn new() -> GetSessionStateRequest {
        GetSessionStateRequest::new()
    }

    fn descriptor_static(_: ::std::option::Option<GetSessionStateRequest>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "session_id",
                    GetSessionStateRequest::get_session_id_for_reflect,
                    GetSessionStateRequest::mut_session_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "step_index",
                    GetSessionStateRequest::get_step_index_for_reflect,
                    GetSessionStateRequest::mut_step_index_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<GetSessionStateRequest>(
                    "GetSessionStateRequest",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for GetSessionStateRequest {
    fn clear(&mut self) {
        self.clear_session_id();
        self.clear_step_index();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for GetSessionStateRequest {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for GetSessionStateRequest {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Register {
    // message fields
    pub name: ::std::string::String,
    pub value: u64,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Register {}

impl Register {
    pub fn new() -> Register {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Register {
        static mut instance: ::protobuf::lazy::Lazy<Register> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Register,
        };
        unsafe {
            instance.get(Register::new)
        }
    }

    // string name = 1;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        &mut self.name
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.name, ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    fn get_name_for_reflect(&self) -> &::std::string::String {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.name
    }

    // uint64 value = 2;

    pub fn clear_value(&mut self) {
        self.value = 0;
    }

    // Param is passed by value, moved
    pub fn set_value(&mut self, v: u64) {
        self.value = v;
    }

    pub fn get_value(&self) -> u64 {
        self.value
    }

    fn get_value_for_reflect(&self) -> &u64 {
        &self.value
    }

    fn mut_value_for_reflect(&mut self) -> &mut u64 {
        &mut self.value
    }
}

impl ::protobuf::Message for Register {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.name)?;
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.value = tmp;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.name.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.name);
        }
        if self.value != 0 {
            my_size += ::protobuf::rt::value_size(2, self.value, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.name.is_empty() {
            os.write_string(1, &self.name)?;
        }
        if self.value != 0 {
            os.write_uint64(2, self.value)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Register {
    fn new() -> Register {
        Register::new()
    }

    fn descriptor_static(_: ::std::option::Option<Register>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    Register::get_name_for_reflect,
                    Register::mut_name_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "value",
                    Register::get_value_for_reflect,
                    Register::mut_value_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Register>(
                    "Register",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Register {
    fn clear(&mut self) {
        self.clear_name();
        self.clear_value();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Register {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Register {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct State {
    // message fields
    pub id: ::std::string::String,
    pub registers: ::protobuf::RepeatedField<Register>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for State {}

impl State {
    pub fn new() -> State {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static State {
        static mut instance: ::protobuf::lazy::Lazy<State> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const State,
        };
        unsafe {
            instance.get(State::new)
        }
    }

    // string id = 1;

    pub fn clear_id(&mut self) {
        self.id.clear();
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: ::std::string::String) {
        self.id = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_id(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // Take field
    pub fn take_id(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.id, ::std::string::String::new())
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    fn get_id_for_reflect(&self) -> &::std::string::String {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::string::String {
        &mut self.id
    }

    // repeated .tdb.Register registers = 2;

    pub fn clear_registers(&mut self) {
        self.registers.clear();
    }

    // Param is passed by value, moved
    pub fn set_registers(&mut self, v: ::protobuf::RepeatedField<Register>) {
        self.registers = v;
    }

    // Mutable pointer to the field.
    pub fn mut_registers(&mut self) -> &mut ::protobuf::RepeatedField<Register> {
        &mut self.registers
    }

    // Take field
    pub fn take_registers(&mut self) -> ::protobuf::RepeatedField<Register> {
        ::std::mem::replace(&mut self.registers, ::protobuf::RepeatedField::new())
    }

    pub fn get_registers(&self) -> &[Register] {
        &self.registers
    }

    fn get_registers_for_reflect(&self) -> &::protobuf::RepeatedField<Register> {
        &self.registers
    }

    fn mut_registers_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<Register> {
        &mut self.registers
    }
}

impl ::protobuf::Message for State {
    fn is_initialized(&self) -> bool {
        for v in &self.registers {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.id)?;
                },
                2 => {
                    ::protobuf::rt::read_repeated_message_into(wire_type, is, &mut self.registers)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.id.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.id);
        }
        for value in &self.registers {
            let len = value.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if !self.id.is_empty() {
            os.write_string(1, &self.id)?;
        }
        for v in &self.registers {
            os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for State {
    fn new() -> State {
        State::new()
    }

    fn descriptor_static(_: ::std::option::Option<State>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "id",
                    State::get_id_for_reflect,
                    State::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Register>>(
                    "registers",
                    State::get_registers_for_reflect,
                    State::mut_registers_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<State>(
                    "State",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for State {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_registers();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for State {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for State {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\ttdb.proto\x12\x03tdb\x1a\nfile.proto\x1a\x0cbinary.proto\"#\n\x11Get\
    SessionRequest\x12\x0e\n\x02id\x18\x01\x20\x01(\tR\x02id\">\n\x14CreateS\
    essionRequest\x12&\n\x07session\x18\x01\x20\x01(\x0b2\x0c.tdb.SessionR\
    \x07session\"\xb5\x01\n\rSessionConfig\x12\x10\n\x03brk\x18\x01\x20\x01(\
    \x04R\x03brk\x12!\n\x0cinternal_brk\x18\x02\x20\x01(\x04R\x0binternalBrk\
    \x12\x1d\n\x04bits\x18\x03\x20\x01(\x0e2\t.tdb.BitsR\x04bits\x12#\n\x06e\
    ndian\x18\x04\x20\x01(\x0e2\x0b.tdb.EndianR\x06endian\x12+\n\x11program_\
    arguments\x18\x05\x20\x03(\tR\x10programArguments\"f\n\x07Session\x12\
    \x0e\n\x02id\x18\x01\x20\x01(\tR\x02id\x12*\n\x06config\x18\x02\x20\x01(\
    \x0b2\x12.tdb.SessionConfigR\x06config\x12\x1f\n\x0bsteps_count\x18\x03\
    \x20\x01(\x04R\nstepsCount\"V\n\x16GetSessionStateRequest\x12\x1d\n\nses\
    sion_id\x18\x01\x20\x01(\tR\tsessionId\x12\x1d\n\nstep_index\x18\x02\x20\
    \x01(\x04R\tstepIndex\"4\n\x08Register\x12\x12\n\x04name\x18\x01\x20\x01\
    (\tR\x04name\x12\x14\n\x05value\x18\x02\x20\x01(\x04R\x05value\"D\n\x05S\
    tate\x12\x0e\n\x02id\x18\x01\x20\x01(\tR\x02id\x12+\n\tregisters\x18\x02\
    \x20\x03(\x0b2\r.tdb.RegisterR\tregisters2\xc0\x01\n\x0eSessionManager\
    \x12:\n\rCreateSession\x12\x19.tdb.CreateSessionRequest\x1a\x0c.tdb.Sess\
    ion\"\0\x124\n\nGetSession\x12\x16.tdb.GetSessionRequest\x1a\x0c.tdb.Ses\
    sion\"\0\x12<\n\x0fGetSessionState\x12\x1b.tdb.GetSessionStateRequest\
    \x1a\n.tdb.State\"\0b\x06proto3\
";

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
