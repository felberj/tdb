//
//  HexFormatter.swift
//  tdbui
//
//  Created by Jonas Felber on 20.01.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

class HexFormatter: Formatter {
    override func string(for obj: Any?) -> String? {
        guard let num = obj as? Int else {
            return "??"
        }
        return  String(format:"0x%x", num)
    }
}
