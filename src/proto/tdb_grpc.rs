// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]


// interface

pub trait SessionManager {
    fn create_session(&self, o: ::grpc::RequestOptions, p: super::tdb::CreateSessionRequest) -> ::grpc::SingleResponse<super::tdb::Session>;

    fn get_session(&self, o: ::grpc::RequestOptions, p: super::tdb::GetSessionRequest) -> ::grpc::SingleResponse<super::tdb::Session>;

    fn get_session_state(&self, o: ::grpc::RequestOptions, p: super::tdb::GetSessionStateRequest) -> ::grpc::SingleResponse<super::tdb::State>;
}

// client

pub struct SessionManagerClient {
    grpc_client: ::grpc::Client,
    method_CreateSession: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::tdb::CreateSessionRequest, super::tdb::Session>>,
    method_GetSession: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::tdb::GetSessionRequest, super::tdb::Session>>,
    method_GetSessionState: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::tdb::GetSessionStateRequest, super::tdb::State>>,
}

impl SessionManagerClient {
    pub fn with_client(grpc_client: ::grpc::Client) -> Self {
        SessionManagerClient {
            grpc_client: grpc_client,
            method_CreateSession: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/tdb.SessionManager/CreateSession".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
            method_GetSession: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/tdb.SessionManager/GetSession".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
            method_GetSessionState: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/tdb.SessionManager/GetSessionState".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
        }
    }

    pub fn new_plain(host: &str, port: u16, conf: ::grpc::ClientConf) -> ::grpc::Result<Self> {
        ::grpc::Client::new_plain(host, port, conf).map(|c| {
            SessionManagerClient::with_client(c)
        })
    }
    pub fn new_tls<C : ::tls_api::TlsConnector>(host: &str, port: u16, conf: ::grpc::ClientConf) -> ::grpc::Result<Self> {
        ::grpc::Client::new_tls::<C>(host, port, conf).map(|c| {
            SessionManagerClient::with_client(c)
        })
    }
}

impl SessionManager for SessionManagerClient {
    fn create_session(&self, o: ::grpc::RequestOptions, p: super::tdb::CreateSessionRequest) -> ::grpc::SingleResponse<super::tdb::Session> {
        self.grpc_client.call_unary(o, p, self.method_CreateSession.clone())
    }

    fn get_session(&self, o: ::grpc::RequestOptions, p: super::tdb::GetSessionRequest) -> ::grpc::SingleResponse<super::tdb::Session> {
        self.grpc_client.call_unary(o, p, self.method_GetSession.clone())
    }

    fn get_session_state(&self, o: ::grpc::RequestOptions, p: super::tdb::GetSessionStateRequest) -> ::grpc::SingleResponse<super::tdb::State> {
        self.grpc_client.call_unary(o, p, self.method_GetSessionState.clone())
    }
}

// server

pub struct SessionManagerServer;


impl SessionManagerServer {
    pub fn new_service_def<H : SessionManager + 'static + Sync + Send + 'static>(handler: H) -> ::grpc::rt::ServerServiceDefinition {
        let handler_arc = ::std::sync::Arc::new(handler);
        ::grpc::rt::ServerServiceDefinition::new("/tdb.SessionManager",
            vec![
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/tdb.SessionManager/CreateSession".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.create_session(o, p))
                    },
                ),
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/tdb.SessionManager/GetSession".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.get_session(o, p))
                    },
                ),
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/tdb.SessionManager/GetSessionState".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.get_session_state(o, p))
                    },
                ),
            ],
        )
    }
}
