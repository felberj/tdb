// `error_chain!` can recurse deeply
#![recursion_limit = "1024"]
#[macro_use]
extern crate error_chain;
extern crate byteorder;
extern crate capstone;
extern crate libc;
extern crate protobuf;
extern crate unicorn;

pub mod error;
pub mod structs;
pub mod binary;
pub mod file;
pub use error::*;

pub mod proto;
pub mod libtest;
pub mod libload;
pub mod libkernel;
pub mod libtdb;
pub mod ctdb;
