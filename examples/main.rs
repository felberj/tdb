extern crate tdb;
// [macro_use]
extern crate error_chain;

use file;
use libtdb::Tdb;
use tdb::*;

fn main() {
    let elf_path = "testdata/lib64/ld-linux-x86-64.so.2";

    let elf = binary::parse_elf_file(elf_path).unwrap();
    let options = libtdb::Options {
        program_arguments: vec![String::from("./executable2")],
        ..Default::default()
    };
    let mut tdb = Tdb::new(&elf, options).unwrap();

    tdb.sys
        .process
        .fs
        .load_real_file("testdata/static/dynamic_x86_64_print_args",
                        "./executable2",
                        file::Stat {
                            readable: true,
                            executable: true,
                            ..Default::default()
                        })
        .unwrap();
    tdb.sys
        .process
        .fs
        .load_real_file("testdata/lib64/libc.so.6",
                        "/lib/x86_64-linux-gnu/libc.so.6",
                        file::Stat {
                            readable: true,
                            executable: true,
                            ..Default::default()
                        })
        .unwrap();
    libload::load_elf(&mut tdb.sys.process, &mut tdb.cpu, &elf).unwrap();
    tdb.start().unwrap();
}
