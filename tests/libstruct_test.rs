extern crate tdb;

use tdb::libtest::T;
use tdb::structs::*;
use tdb::binary::{Bits, Endianness};

#[test]
fn test_u8_pack() {
    let mut t: T = Default::default();

    {
        let val = Struct::U8("", 0);
        let got = val.pack(Bits::Bits32, Endianness::Little).unwrap();
        let want: &[u8] = &[0];
        if got != want {
            t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
        }
    }
    {
        let val = Struct::U8("", 100);
        let got = val.pack(Bits::Bits32, Endianness::Little).unwrap();
        let want: &[u8] = &[100];
        if got != want {
            t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
        }
    }
}

#[test]
fn test_i8_pack() {
    let mut t: T = Default::default();

    {
        let val = Struct::I8("", 0);
        let got = val.pack(Bits::Bits32, Endianness::Little).unwrap();
        let want: &[u8] = &[0];
        if got != want {
            t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
        }
    }
    {
        let val = Struct::I8("", -1);
        let got = val.pack(Bits::Bits32, Endianness::Little).unwrap();
        let want: &[u8] = &[0xff];
        if got != want {
            t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
        }
    }
}

#[test]
fn test_iword_pack() {
    let mut t: T = Default::default();
    let val = Struct::Iword("", -1);
    let got = val.pack(Bits::Bits32, Endianness::Little).unwrap();
    let want: &[u8] = &[0xff, 0xff, 0xff, 0xff];
    if got != want {
        t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
    }
}

#[test]
fn test_pack_simple_struct() {
    let mut t: T = Default::default();

    let mut vals: Vec<Struct> = Vec::new();
    vals.push(Struct::U8("", 0));
    vals.push(Struct::U8("", 10));
    vals.push(Struct::U64("", 0xaabbccddeeff));
    let val = Struct::Struct("", vals);
    let got = val.pack(Bits::Bits64, Endianness::Little).unwrap();
    let want: &[u8] = &[0, 10, 0, 0, 0, 0, 0, 0, 0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0, 0];
    if got != want {
        t.error(format!("pack({:?}) failed, got {:?}, want {:?}", val, got, want));
    }
}

#[test]
fn test_unpack_simple_struct() {
    let mut t: T = Default::default();

    let mut vals: Vec<Struct> = Vec::new();
    vals.push(Struct::U8("null", 1));
    vals.push(Struct::U8("ten", 1));
    vals.push(Struct::U64("dead", 1));
    let mut val = Struct::Struct("", vals);
    let data: &[u8] = &[0, 10, 0, 0, 0, 0, 0, 0, 0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0, 0];
    val.unpack(Bits::Bits64, Endianness::Little, data).unwrap();
    if val.get("null").unwrap().to_u64().unwrap() != 0 {
        t.error(format!("unpack('null') failed, val {}, want {}", val.get("null").unwrap().to_u64().unwrap(), 0));
    }
    if val.get("ten").unwrap().to_u64().unwrap() != 10 {
        t.error(format!("unpack('ten') failed, val {}, want {}", val.get("ten").unwrap().to_u64().unwrap(), 10));
    }
    let want = 0xaabbccddeeff;
    if val.get("dead").unwrap().to_u64().unwrap() != want {
        t.error(format!("unpack('dead') failed, val {}, want {}", val.get("dead").unwrap().to_u64().unwrap(), want));
    }
}

#[test]
fn test_get_name() {
    let mut t: T = Default::default();

    let mut vals: Vec<Struct> = Vec::new();
    vals.push(Struct::U8("null", 0));
    vals.push(Struct::U8("ten", 10));
    vals.push(Struct::U64("dead", 0xaabbccddeeff));
    let val = Struct::Struct("", vals);
    {
        let got = val.get("null").unwrap().to_u64().unwrap();
        let want = 0;
        if got != want {
            t.error(format!("pack('null') failed, got {:?}, want {:?}", got, want));
        }
    }
    {
        let got = val.get("dead").unwrap().to_u64().unwrap();
        let want = 0xaabbccddeeff;
        if got != want {
            t.error(format!("pack('null') failed, got {:?}, want {:?}", got, want));
        }
    }
}
