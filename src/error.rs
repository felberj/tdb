use capstone;
use std;
use std::result;
use unicorn;

error_chain!{
        foreign_links {
            Io(std::io::Error);
            Time(std::time::SystemTimeError);
            Utf8(std::str::Utf8Error);
        }
    }
impl From<unicorn::Error> for Error {
    fn from(e: unicorn::Error) -> Error {
        format!("Unicorn error: {:?}", e).into()
    }
}
impl From<capstone::Error> for Error {
    fn from(e: capstone::Error) -> Error {
        format!("Capstone error: {:?}", e).into()
    }
}

#[must_use]
pub type TdbResult<T> = result::Result<T, Error>;
