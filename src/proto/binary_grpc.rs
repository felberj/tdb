// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]


// interface

pub trait BinaryManager {
    fn create_binary(&self, o: ::grpc::RequestOptions, p: super::binary::CreateBinaryRequest) -> ::grpc::SingleResponse<super::binary::Binary>;
}

// client

pub struct BinaryManagerClient {
    grpc_client: ::grpc::Client,
    method_CreateBinary: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::binary::CreateBinaryRequest, super::binary::Binary>>,
}

impl BinaryManagerClient {
    pub fn with_client(grpc_client: ::grpc::Client) -> Self {
        BinaryManagerClient {
            grpc_client: grpc_client,
            method_CreateBinary: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/tdb.BinaryManager/CreateBinary".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
        }
    }

    pub fn new_plain(host: &str, port: u16, conf: ::grpc::ClientConf) -> ::grpc::Result<Self> {
        ::grpc::Client::new_plain(host, port, conf).map(|c| {
            BinaryManagerClient::with_client(c)
        })
    }
    pub fn new_tls<C : ::tls_api::TlsConnector>(host: &str, port: u16, conf: ::grpc::ClientConf) -> ::grpc::Result<Self> {
        ::grpc::Client::new_tls::<C>(host, port, conf).map(|c| {
            BinaryManagerClient::with_client(c)
        })
    }
}

impl BinaryManager for BinaryManagerClient {
    fn create_binary(&self, o: ::grpc::RequestOptions, p: super::binary::CreateBinaryRequest) -> ::grpc::SingleResponse<super::binary::Binary> {
        self.grpc_client.call_unary(o, p, self.method_CreateBinary.clone())
    }
}

// server

pub struct BinaryManagerServer;


impl BinaryManagerServer {
    pub fn new_service_def<H : BinaryManager + 'static + Sync + Send + 'static>(handler: H) -> ::grpc::rt::ServerServiceDefinition {
        let handler_arc = ::std::sync::Arc::new(handler);
        ::grpc::rt::ServerServiceDefinition::new("/tdb.BinaryManager",
            vec![
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/tdb.BinaryManager/CreateBinary".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.create_binary(o, p))
                    },
                ),
            ],
        )
    }
}
