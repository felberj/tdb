//
//  Syscall.swift
//  tdbui
//
//  Created by Jonas Felber on 21.01.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

class Syscall: NSObject {
    @objc dynamic var time: UInt64
    @objc dynamic var name: String
    
    init(time: UInt64, name: String) {
        self.name = name
        self.time = time
    }
}
