use std::ops::Drop;

#[derive(Default)]
pub struct T {
    failed: bool,
}

impl T {
    pub fn error<S>(&mut self, msg: S)
        where S: Into<String>
    {
        self.failed = true;
        println!("{}", msg.into());
    }

    pub fn log(&self, msg: &str) {
        println!("{}", msg);
    }
}

impl Drop for T {
    fn drop(&mut self) {
        if self.failed {
            panic!();
        }
    }
}

#[cfg(test)]
mod test {

    use super::T;

    #[test]
    fn test_no_error() {
        let t = T { failed: false };
        t.log("hello")
    }

    #[test]
    #[should_panic]
    fn test_with_error() {
        let mut t = T { failed: false };
        t.error("hello")
    }
}
