//
//  Register.swift
//  tdbui
//
//  Created by Jonas Felber on 04.02.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

class Register: NSObject {
    @objc dynamic var name: String
    @objc dynamic var value: UInt64
    
    init(name: String, value: UInt64) {
        self.name = name
        self.value = value
        super.init()
    }
}
