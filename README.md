# TDB - Timeless Debugger

## Goal

We want a better debugger for CTF binaries. Because of that, we will mainly
focus on ELF binaries and the most common syscalls. In the future it would be
nice to have a python interface to it to make it scriptable.

## Roadmap

see Issue #2

### Elf parsing

- [x] parse elf header
- [ ] parse program headers

### Elf loading

- [ ] create a structure that represents a process
- [ ] 'map' the Elf into memory
- [ ] if loader specified, load the loader

### Run the elf

- [ ] figure out how to use the unicorn bindings
- [ ] run a static elf until we hit a syscall
- [ ] implement single stepping
- [ ] figure out how to create a diff after each instruction

### Emulate Kernel

- [ ] figure out how to emulate the kernel
- [ ] implement the needed syscalls to run a static hello world binary

### Python Bindings

- [ ] add python bindings

## Related Projects

- https://github.com/lunixbochs/usercorn