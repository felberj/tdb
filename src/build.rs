//extern crate cheddar;
extern crate protoc_rust_grpc;

fn main() {
    /*    for lib in &["binary", "libtdb", "ctdb"] {
        cheddar::Cheddar::new()
            .expect("could not read manifest")
            .source_file(format!{"src/{}.rs", lib})
            .run_build(format!{"include/{}.h", lib});
}*/
    protoc_rust_grpc::run(protoc_rust_grpc::Args {
        out_dir: "src/proto",
        includes: &["proto"],
        input: &["proto/tdb.proto", "proto/binary.proto"],
        rust_protobuf: true, // also generate protobuf messages, not just services
        ..Default::default()
    }).expect("protoc-rust-grpc");
}
