//
//  ViewController.swift
//  tdbui
//
//  Created by Jonas Felber on 20.01.18.
//  Copyright © 2018 Jonas Felber. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var fileField: NSTextField!
    @IBOutlet var instructionsController: NSArrayController!
    @IBOutlet var syscallController: NSArrayController!
    @IBOutlet weak var syscallTable: NSTableView!
    @IBOutlet weak var instructionTable: NSTableView!
    @IBOutlet weak var registerTable: NSTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func playClicked(_ sender: Any) {
        if fileField.stringValue == "" {
            print("No file selected")
        }
        let file = fileField.stringValue
        print("try to open \(file)")
        var ok: Int32 = 0
        let elf = libelf_parse_elf(file, &ok)
        if ok == 0 {
            print("parsing elf failed")
            return;
        }
        let options = libtdb_options_new()
        libtdb_options_add_program_argument(options, "Hello World!")
        // Void pointer to `self`:
        // FIXME: memory leak
        let observer = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        libtdb_options_set_instruction_executed_callback(options, observer, { (observer, instr) -> Void in
            if let observer = observer {
                let instruction = Instruction(time: instr.time, address: instr.address, instruction: String(cString: instr.op_str), registers: [])
                let user_data = UnsafeMutableRawPointer(Unmanaged.passUnretained(instruction).toOpaque())
                libtdb_instruction_collect_registers(user_data, instr, { (user_data, name, value) -> Void in
                    if let user_data = user_data, let name = name {
                        let instr = Unmanaged<Instruction>.fromOpaque(user_data).takeUnretainedValue()
                        instr.registers.append(Register(name: String(cString: name), value: value))
                    }
                })
                instruction.registers.sort {$0.name > $1.name }
                // FIXME bad, as we fill up queue
                DispatchQueue.main.async {
                    let mySelf = Unmanaged<ViewController>.fromOpaque(observer).takeUnretainedValue()
                    mySelf.instructionExecuted(instruction)
                }
            }
        })
        libtdb_options_set_syscall_executed_callback(options, observer, { (observer, time, name) -> Void in
            if let observer = observer, let name = name {
                let sys = Syscall(time: time, name: String(cString: name))
                // FIXME bad, as we fill up queue
                DispatchQueue.main.async {
                    let mySelf = Unmanaged<ViewController>.fromOpaque(observer).takeUnretainedValue()
                    mySelf.syscallExecuted(sys)
                }
            }
        })
        
        let tdb = libtdb_tdb_new(elf, options, &ok)
        if ok == 0 {
            print("open tdb failed")
            return
        }
        ok = libtdb_tdb_load_elf(tdb, elf)
        if ok == 0 {
            print("load failed")
            return
        }
       
        DispatchQueue.global(qos: .background).async {
            libtdb_tdb_start(tdb)
            print("tdb finished")
        }
    }
    
    func instructionExecuted(_ inst: Instruction) {
        assert(Thread.isMainThread)
        instructionsController.addObject(inst)
    }
    
    func syscallExecuted(_ sys: Syscall) {
        assert(Thread.isMainThread)
        syscallController.addObject(sys)
    }
    
    @IBAction func openClicked(_ sender: Any) {
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose a binary";
        dialog.canChooseDirectories    = false;
        dialog.allowsMultipleSelection = false;
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            guard let result = dialog.url else {
                return
            }
            fileField.stringValue = result.path
        } else {
            // User clicked on "Cancel"
            return
        }
    }
    @IBAction func syscallClicked(_ sender: Any) {
        let sys = (syscallController.arrangedObjects as! [Syscall])[syscallTable.selectedRow]
        for (i, instr) in (instructionsController.arrangedObjects as! [Instruction]).enumerated() {
            if sys.time == instr.time {
                instructionTable.selectRowIndexes(IndexSet(integer: i), byExtendingSelection: false)
                instructionTable.scrollRowToVisible(i)
                break
            }
        }
    }

}

extension ViewController: NSTableViewDelegate {
    func tableViewSelectionDidChange(_ notification: Notification) {
        // ... whatever else ...
        print("a instruction changed")
        let tableView = notification.object as! NSTableView
        if tableView == instructionTable {
            print("instruction changed")
        }
    }
}

