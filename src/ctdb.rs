use binary::Elf;
use libc;
use libload;
use libtdb::{Instruction as Instruction_, Options, Tdb};
use std::{ffi, marker, ptr};

#[no_mangle]
#[repr(C)]
pub struct TdbInstruction {
    pub time: u64,                       // Time when the instruction was executed
    pub address: u64,                    // Address of the instruction
    pub op_str: *const libc::c_char,     // Ascii text of instruction operands
    pub ins_handle: *const Instruction_, // pointer to Instructions
}

pub type CallbackHandle = *mut libc::c_void;

pub type InstructionExecutedCallback = extern "C" fn(CallbackHandle, TdbInstruction);
pub type CollectRegistersCallback = extern "C" fn(*mut libc::c_void, *const libc::c_char, u64);

// FIXME: Ensure memory cleanup
pub struct ExecutionCallback {
    pub handle: CallbackHandle,
    pub callback: InstructionExecutedCallback,
}
unsafe impl marker::Send for ExecutionCallback {}

pub type SyscallExecutedCallback = extern "C" fn(CallbackHandle, u64, *const libc::c_char);

// FIXME: Ensure memory cleanup
pub struct SyscallCallback {
    pub handle: CallbackHandle,
    pub callback: SyscallExecutedCallback,
}
unsafe impl marker::Send for SyscallCallback {}

#[no_mangle]
pub extern "C" fn libtdb_options_set_instruction_executed_callback(
    ptr: *mut Options,
    handle: *mut libc::c_void,
    cb: InstructionExecutedCallback,
) {
    let o = unsafe { &mut *ptr };
    let c = ExecutionCallback {
        handle: handle,
        callback: cb,
    };
    o.ecb = Some(Box::new(move |instr| {
        let cb = c.callback;
        cb(
            c.handle,
            TdbInstruction {
                time: instr.time,
                address: instr.address,
                op_str: instr.repr.as_c_str().as_ptr(),
                ins_handle: instr,
            },
        );
    }))
}

#[no_mangle]
pub extern "C" fn libtdb_instruction_collect_registers(
    user_data: *mut libc::c_void,
    instr: TdbInstruction,
    cb: CollectRegistersCallback,
) {
    let i = unsafe { &*instr.ins_handle };
    for (reg, val) in i.regs.iter() {
        let c = ffi::CString::new(reg.clone()).unwrap();
        cb(user_data, c.as_c_str().as_ptr(), *val);
    }
}

#[no_mangle]
pub extern "C" fn libtdb_options_set_syscall_executed_callback(
    ptr: *mut Options,
    handle: *mut libc::c_void,
    cb: SyscallExecutedCallback,
) {
    let o = unsafe { &mut *ptr };
    let c = SyscallCallback {
        handle: handle,
        callback: cb,
    };
    o.scb = Some(Box::new(move |time, name| {
        let cb = c.callback;
        let cstr = ffi::CString::new(name).unwrap();
        cb(c.handle, time, cstr.as_c_str().as_ptr());
    }))
}

#[no_mangle]
pub extern "C" fn libtdb_options_new() -> *mut Options {
    Box::into_raw(Box::new(Options {
        ..Default::default()
    }))
}

#[no_mangle]
pub extern "C" fn libtdb_options_add_program_argument(
    ptr: *mut Options,
    option: *const libc::c_char,
) {
    let c_str = unsafe { ffi::CStr::from_ptr(option) };
    let argument = c_str.to_owned().into_string().unwrap();
    let o = unsafe { &mut *ptr };
    o.program_arguments.push(argument);
}

#[no_mangle]
pub extern "C" fn libtdb_tdb_new(
    elf: *const Elf,
    opt: *mut Options,
    ok: *mut libc::c_int,
) -> *mut Tdb {
    let e = unsafe { &*elf };
    let o = unsafe { Box::from_raw(opt) };
    match Tdb::new(&e, *o) {
        Ok(tdb) => {
            unsafe { *ok = 1 };
            return Box::into_raw(Box::new(tdb));
        }
        Err(_) => {
            unsafe { *ok = 0 };
            return ptr::null_mut();
        }
    }
}

#[no_mangle]
pub extern "C" fn libtdb_tdb_start(tdb: *mut Tdb) -> libc::c_int {
    let t = unsafe { &mut *tdb };
    match t.start() {
        Ok(_) => {
            return 1;
        }
        Err(e) => {
            println!{"Error {}", e};
            return 0;
        }
    }
}

#[no_mangle]
// libtdb_tdb_load-elf loads the elf file into memory
pub extern "C" fn libtdb_tdb_load_elf(tdb: *mut Tdb, elf: *const Elf) -> libc::c_int {
    let e = unsafe { &*elf };
    let t = unsafe { &mut *tdb };
    match libload::load_elf(&mut t.sys.process, &mut t.cpu, e) {
        Ok(_) => {
            return 1;
        }
        Err(e) => {
            println!{"Error {}", e};
            return 0;
        }
    }
}
