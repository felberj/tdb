Builds two simple statically linked binaries in both `x86` and `x86_64` mode.

    make

Requires libc to be installed for both architectures.
