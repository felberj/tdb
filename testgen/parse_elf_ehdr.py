import subprocess
import re

readelf = 'greadelf' # osx, sorry


def generate_test(i, f):
    out = subprocess.check_output([readelf, '-h', f]) 
    for line in out.split('\n'):
        line = line.strip()
        if not ':' in line:
            continue
        [key, value] = line.split(':', 1)
        value = re.sub("\(.*\)", "", value)
        value = value.strip()
        if key == 'Class':
            if '32' in value:
                bits = 'Bits::Bits32'
            else:
                bits = 'Bits::Bits64'
        elif key == 'Machine':
            pass # todo
        elif key == 'Entry point address':
            e_entry = value
        elif key == 'Start of program headers':
            e_phoff = value
        elif key == 'Start of section headers':
            e_shoff = value
        elif key == 'Size of program headers':
            e_phentsize = value
        elif key == 'Number of program headers':
            e_phnum = value
        elif key == 'Size of section headers':
            # print('assert_eq!(%s, ehdr.e_shentsize);' % value)
            pass # todo
        elif key == 'Number of section headers':
            e_shnum = value
    print(" TestCase{file: \"%s\", want: Ehdr{ e_ident: ElfIdent{bits: %s, endianness: Endianness::Big},  e_entry: %s, e_phoff: %s, e_shoff: %s, e_phentsize: %s, e_phnum: %s, e_shnum: %s}}," % (f, bits, e_entry, e_phoff, e_shoff, e_phentsize, e_phnum, e_shnum))

binaries = ['testdata/elf_x86_64_keyval.out', 'testdata/static/static_x86', 'testdata/static/static_x86_64', 'testdata/static/dynamic_x86_64_print_args', 'testdata/lib64/ld-linux-x86-64.so.2']

for i, f in enumerate(binaries):
    generate_test(i, f)
