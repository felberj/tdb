#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#include "../include/types.h"
#include "../include/binary.h"
#include "../include/libtdb.h"

void* run(Tdb* tdb) {
  libtdb_tdb_start(tdb);
  return NULL;
}

int main(void) {
  int ok;
  Elf* elf = libelf_parse_elf("../testdata/static/static_x86_print_args", &ok);
  if (!ok) {
    return 1;
  }
  Options* options = libtdb_options_new();
  libtdb_options_add_program_argument(options, "Hello World!");

  Tdb* tdb = libtdb_tdb_new(elf, options, &ok);
  if (!ok) {
    return 2;
  }
  ok = libtdb_tdb_load_elf(tdb, elf);
  if (!ok) {
    return 3;
  }
  pthread_t tid;
  pthread_create(&tid, NULL, run, tdb);
  sleep(1);
  libtdb_tdb_query_execution(tdb, 0, 100);
  return 0;
}
