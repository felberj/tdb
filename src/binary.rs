// http://wiki.osdev.org/ELF

use byteorder::{BigEndian, LittleEndian, ReadBytesExt};
use error::Result;
use libc;
pub use proto::binary::{Bits, ElfType, Endian, Machine};
use std::ffi;
use std::fs::File;
use std::io::Cursor;
use std::io::SeekFrom;
use std::io::prelude::*;
use std::ptr;
use structs::Struct;

#[repr(C)]
pub struct ElfIdent {
    pub bits: Bits,
    pub endian: Endian,
}

// Section types. Only interesting ones were added here.
#[repr(C)]
pub enum SectionType {
    // Symtab = 2, // Symbol table.
    // Strtab = 3, // String table.
    Rela = 4, // Relocation entries; explicit addends.
    Rel = 9,  /* Relocation entries; no explicit addends.
               * Dynsym = 11, // Symbol table. */
}

#[repr(C)]
pub struct Section {
    // name: String, // Section name (index into string table)
    pub s_type: SectionType,
    // Elf32_Word sh_flags;     // Section flags (SHF_*)
    // addr: u64, // Address where section is to be loaded
    pub offset: u64, // File offset of section data, in bytes
    pub size: usize, // Size of section, in bytes
    // Elf32_Word sh_link;      // Section type-specific header table index link
    // Elf32_Word sh_info;      // Section type-specific extra information
    // Elf32_Word sh_addralign; // Section address alignment
    pub entsize: usize, // Size of records contained within the section
}

#[repr(C)]
pub struct Ehdr {
    pub e_ident: ElfIdent,
    pub e_type: ElfType,  // Type of file
    pub e_entry: u64,     // Address to jump to in order to start program
    pub e_phoff: u64,     // Program header table's file offset, in bytes
    pub e_phentsize: u64, // Size of an entry in the program header table
    pub e_phnum: u64,     // Number of entries in the program header table
    pub e_shoff: usize,   // Section header table's file offset, in bytes
    pub e_shnum: usize,   // Number of entries in the section header table
    pub e_shentsize: usize,
    pub machine: Machine,
}

#[repr(C)]
pub struct Elf {
    pub name: String,
    pub ident: ElfIdent,
    pub ehdr: Ehdr,
    pub phdrs: Vec<Phdr>,
    pub sections: Vec<Section>,
    pub data: Vec<u8>,
}

#[no_mangle]
pub extern "C" fn libelf_parse_elf(
    elf_path: *const libc::c_char,
    ok: *mut libc::c_int,
) -> *mut Elf {
    let c_str = unsafe { ffi::CStr::from_ptr(elf_path) };
    let argument = c_str.to_str().unwrap();
    match parse_elf_file(argument) {
        Ok(elf) => {
            unsafe { *ok = 1 };
            return Box::into_raw(Box::new(elf));
        }
        Err(err) => {
            println!{"{}", err};
            unsafe { *ok = 0 };
            return ptr::null_mut();
        }
    }
}

pub fn parse_elf_file(elf_path: &str) -> Result<Elf> {
    let mut file = File::open(elf_path)?;
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents)?;

    parse_elf(contents).map(|mut elf| {
        elf.name = String::from(elf_path);
        elf
    })
}

pub fn parse_elf(data: Vec<u8>) -> Result<Elf> {
    let ident = try!(parse_elf_ident(data.as_slice()));
    let ehdr = try!(parse_ehdr(data.as_slice()));
    let phdrs = try!(parse_program_headers(data.as_slice(), &ehdr));
    let sections = try!(parse_sections(data.as_slice(), &ehdr));
    return Ok(Elf {
        name: String::from(""),
        data: data,
        ident: ident,
        ehdr: ehdr,
        phdrs: phdrs,
        sections: sections,
    });
}

fn parse_elf_ident(indent: &[u8]) -> Result<ElfIdent> {
    if indent.len() < EI_NIDENT || !is_elf(&indent) {
        bail!("Invalid elf magic.");
    }
    let endian = match indent[EI_DATA] {
        1 => Endian::LITTLE,
        2 => Endian::BIG,
        _ => bail!("Invalid endian."),
    };
    let bits = match indent[EI_CLASS] {
        1 => Bits::BITS_32,
        2 => Bits::BITS_64,
        _ => bail!("Invalid elf class."),
    };
    Ok(ElfIdent {
        bits: bits,
        endian: endian,
    })
}

pub fn parse_ehdr(data: &[u8]) -> Result<Ehdr> {
    let e_ident = try!(parse_elf_ident(&data));
    let little = e_ident.endian == Endian::LITTLE;
    let offsets = ehdr_offsets(e_ident.bits);
    let mut buff = Cursor::new(data);
    let seek = |buff: &mut Cursor<&[u8]>, to| buff.seek(SeekFrom::Start(to));

    try!(seek(&mut buff, offsets.e_type));
    let _e_type = read_16(&mut buff, little);
    let e_type = match _e_type {
        0 => ElfType::NONE,
        1 => ElfType::REL,
        2 => ElfType::EXEC,
        3 => ElfType::DYN,
        4 => ElfType::CORE,
        _ => bail!("Invalid elf type: {}", _e_type),
    };

    try!(seek(&mut buff, offsets.e_machine));
    let _machine = read_16(&mut buff, little);
    // // http://www.sco.com/developers/gabi/latest/ch4.eheader.html
    let machine = match _machine {
        3 => Machine::X86,
        62 => Machine::X86_64,
        _ => bail!{"unsupported machine: {}", _machine},
    };

    try!(seek(&mut buff, offsets.e_entry));
    let entry = read_address(&mut buff, little, e_ident.bits);

    try!(seek(&mut buff, offsets.e_phoff));
    let phoff = read_address(&mut buff, little, e_ident.bits);

    try!(seek(&mut buff, offsets.e_shoff));
    let e_shoff = read_address(&mut buff, little, e_ident.bits);

    try!(seek(&mut buff, offsets.e_phentsize));
    let e_phentsize = read_16(&mut buff, little) as u64;

    try!(seek(&mut buff, offsets.e_phnum));
    let e_phnum = read_16(&mut buff, little) as u64;

    try!(seek(&mut buff, offsets.e_shnum));
    let e_shnum = read_16(&mut buff, little) as u64;

    try!(seek(&mut buff, offsets.e_shentsize));
    let e_shentsize = read_16(&mut buff, little) as u64;

    Ok(Ehdr {
        e_ident: e_ident,
        e_type: e_type,
        e_entry: entry,
        e_phoff: phoff,
        e_phnum: e_phnum,
        e_phentsize: e_phentsize,
        e_shoff: e_shoff as usize,
        e_shnum: e_shnum as usize,
        e_shentsize: e_shentsize as usize,
        machine: machine,
    })
}

pub fn parse_program_headers(data: &[u8], ehdr: &Ehdr) -> Result<Vec<Phdr>> {
    if ehdr.e_phnum > 100 {
        // FIXME calculate the correct number
        bail!("No many phdrs.");
    }
    let mut result: Vec<Phdr> = Vec::with_capacity(ehdr.e_phnum as usize);
    for i in 0..ehdr.e_phnum {
        let offset = ehdr.e_phoff + i * ehdr.e_phentsize;
        let phdr = try!(parse_program_header(data, ehdr, offset));
        result.push(phdr);
    }
    return Ok(result);
}

fn parse_program_header(data: &[u8], ehdr: &Ehdr, offset: u64) -> Result<Phdr> {
    let mut buff = Cursor::new(data);
    let offsets = phrdr_offests(ehdr.e_ident.bits);
    let little = ehdr.e_ident.endian == Endian::LITTLE;
    let seek = |buff: &mut Cursor<&[u8]>, to| buff.seek(SeekFrom::Start(offset + to));

    try!(seek(&mut buff, offsets.p_type));
    let _p_type = read_32(&mut buff, little);
    let p_type = match _p_type {
        0 => SegmentType::Null,
        1 => SegmentType::Load,
        2 => SegmentType::Dynamic,
        3 => SegmentType::Interp,
        4 => SegmentType::Note,
        6 => SegmentType::Phdr,
        7 => SegmentType::Tls,
        0x6474e550 => SegmentType::GnuEhFrame,
        0x6474e551 => SegmentType::GnuStack,
        0x6474e552 => SegmentType::GnuRelro,
        _ => bail!("Invalid segment type: {}", _p_type),
    };

    try!(seek(&mut buff, offsets.p_flags));
    let p_flags = read_32(&mut buff, little);
    let readable = (p_flags & 4) == 4;
    let writable = (p_flags & 2) == 2;
    let executable = (p_flags & 1) == 1;

    try!(seek(&mut buff, offsets.p_offset));
    let p_offset = read_address(&mut buff, little, ehdr.e_ident.bits);

    try!(seek(&mut buff, offsets.p_vaddr));
    let p_vaddr = read_address(&mut buff, little, ehdr.e_ident.bits);

    try!(seek(&mut buff, offsets.p_filesz));
    let p_filesz = read_address(&mut buff, little, ehdr.e_ident.bits);

    try!(seek(&mut buff, offsets.p_memsz));
    let p_memsz = read_address(&mut buff, little, ehdr.e_ident.bits);

    try!(seek(&mut buff, offsets.p_align));
    let p_align = read_address(&mut buff, little, ehdr.e_ident.bits);

    return Ok(Phdr {
        p_type: p_type,
        p_offset: p_offset,
        p_filesz: p_filesz,
        p_align: p_align,
        p_vaddr: p_vaddr,
        p_memsz: p_memsz,
        readable: readable,
        writable: writable,
        executable: executable,
    });
}

fn parse_sections(data: &[u8], ehdr: &Ehdr) -> Result<Vec<Section>> {
    let mut s = Vec::new();
    s.push(Struct::U32("sh_name", 0));
    s.push(Struct::U32("sh_type", 0));
    s.push(Struct::Uword("sh_flags", 0));
    s.push(Struct::Uword("sh_addr", 0));
    s.push(Struct::Uword("sh_offset", 0));
    s.push(Struct::Uword("sh_size", 0));
    s.push(Struct::U32("sh_link", 0));
    s.push(Struct::U32("sh_info", 0));
    s.push(Struct::Uword("sh_addralign", 0));
    s.push(Struct::Uword("sh_entsize", 0));
    let mut section = Struct::Struct("", s);
    let size = section.size(ehdr.e_ident.bits);
    if size != ehdr.e_shentsize {
        bail!{"struct has wrong size: expected {} got {}", ehdr.e_shentsize, size};
    }
    let mut res = Vec::new();
    for i in 0..ehdr.e_shnum {
        let s_ptr = ehdr.e_shoff + i * ehdr.e_shentsize;
        section.unpack(
            ehdr.e_ident.bits,
            ehdr.e_ident.endian,
            &data[s_ptr..(s_ptr + ehdr.e_shentsize)],
        )?;
        let t = match section.get("sh_type")?.to_u64()? {
            4 => SectionType::Rela,
            9 => SectionType::Rel,
            _ => continue,
        };
        res.push(Section {
            s_type: t,
            size: section.get("sh_size")?.to_u64()? as usize,
            entsize: section.get("sh_entsize")?.to_u64()? as usize,
            offset: section.get("sh_offset")?.to_u64()?,
        })
    }
    Ok(res)
}

// elf identification bytes
const EI_NIDENT: usize = 16; // Number of bytes in e_ident.
const EI_CLASS: usize = 4; // File class.
const EI_DATA: usize = 5; // Data encoding.
                          //const EI_VERSION: usize = 6;    // File version.
                          //const EI_OSABI: usize = 7;      // OS/ABI identification.
                          //const EI_ABIVERSION: usize = 8; // ABI version.

#[allow(dead_code)]
struct EhdrOffsets {
    e_type: u64,      // Type of file (see ET_* below)
    e_machine: u64,   // Required architecture for this file (see EM_*)
    e_version: u64,   // Must be equal to 1
    e_entry: u64,     // Address to jump to in order to start program
    e_phoff: u64,     // Program header table's file offset, in bytes
    e_shoff: u64,     // Section header table's file offset, in bytes
    e_flags: u64,     // Processor-specific flags
    e_ehsize: u64,    // Size of ELF header, in bytes
    e_phentsize: u64, // Size of an entry in the program header table
    e_phnum: u64,     // Number of entries in the program header table
    e_shentsize: u64, // Size of an entry in the section header table
    e_shnum: u64,     // Number of entries in the section header table
    e_shstrndx: u64,  // Sect hdr table index of sect name string table
}

struct PhdrOffests {
    p_type: u64,
    p_flags: u64,
    p_offset: u64,
    p_vaddr: u64,
    p_filesz: u64,
    p_memsz: u64,
    p_align: u64,
}

#[derive(Debug, PartialEq)]
pub enum SegmentType {
    Null = 0,    // ignore the entry
    Load = 1, /* clear p_memsz bytes at p_vaddr to 0, then copy p_filesz bytes from p_offset to p_vaddr */
    Dynamic = 2, // requires dynamic linking
    Interp = 3, /* contains a file path to an executable to use as an interpreter for the following segment */
    Note = 4,   // note section
    Phdr = 6,   // The program header table itself.
    Tls = 7,    // The thread-local storage template.
    GnuEhFrame = 0x6474e550,
    GnuStack = 0x6474e551,
    GnuRelro = 0x6474e552,
}

pub struct Phdr {
    pub p_type: SegmentType, // Type of segment
    pub p_offset: u64,       // File offset where segment is located, in bytes
    pub p_filesz: u64,       // Num. of bytes in file image of segment (may be zero)
    pub p_vaddr: u64,        // Virtual address of beginning of segment
    pub p_memsz: u64,        // Num. of bytes in mem image of segment (may be zero)
    pub p_align: u64,        // Segment alignment constraint
    pub readable: bool,      // Segment flags
    pub writable: bool,
    pub executable: bool,
}

fn phrdr_offests(bits: Bits) -> PhdrOffests {
    match bits {
        Bits::BITS_32 => PhdrOffests {
            p_type: 0,
            p_flags: 24,
            p_offset: 4,
            p_vaddr: 8,
            p_filesz: 16,
            p_memsz: 20,
            p_align: 28,
        },
        Bits::BITS_64 => PhdrOffests {
            p_type: 0,
            p_flags: 4,
            p_offset: 8,
            p_vaddr: 16,
            p_filesz: 32,
            p_memsz: 40,
            p_align: 48,
        },
    }
}

fn ehdr_offsets(bits: Bits) -> EhdrOffsets {
    match bits {
        Bits::BITS_32 => EhdrOffsets {
            e_type: 16,
            e_machine: 18,
            e_version: 20,
            e_entry: 24,
            e_phoff: 28,
            e_shoff: 32,
            e_flags: 36,
            e_ehsize: 40,
            e_phentsize: 42,
            e_phnum: 44,
            e_shentsize: 46,
            e_shnum: 48,
            e_shstrndx: 50,
        },
        Bits::BITS_64 => EhdrOffsets {
            e_type: 16,
            e_machine: 18,
            e_version: 20,
            e_entry: 24,
            e_phoff: 32,
            e_shoff: 40,
            e_flags: 48,
            e_ehsize: 52,
            e_phentsize: 54,
            e_phnum: 56,
            e_shentsize: 58,
            e_shnum: 60,
            e_shstrndx: 62,
        },
    }
}

fn read_16(f: &mut Cursor<&[u8]>, little: bool) -> u16 {
    if little {
        f.read_u16::<LittleEndian>().unwrap()
    } else {
        f.read_u16::<BigEndian>().unwrap()
    }
}

fn read_32(f: &mut Cursor<&[u8]>, little: bool) -> u32 {
    if little {
        f.read_u32::<LittleEndian>().unwrap()
    } else {
        f.read_u32::<BigEndian>().unwrap()
    }
}

fn read_64(f: &mut Cursor<&[u8]>, little: bool) -> u64 {
    if little {
        f.read_u64::<LittleEndian>().unwrap()
    } else {
        f.read_u64::<BigEndian>().unwrap()
    }
}

fn read_address(f: &mut Cursor<&[u8]>, little: bool, bits: Bits) -> u64 {
    match bits {
        Bits::BITS_32 => read_32(f, little) as u64,
        Bits::BITS_64 => read_64(f, little),
    }
}

fn is_elf(header: &[u8]) -> bool {
    let elf_mag = [0x7f, 0x45, 0x4c, 0x46];
    if header.len() < 4 {
        return false;
    }
    &elf_mag == &header[..elf_mag.len()]
}
