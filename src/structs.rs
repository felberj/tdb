use binary::{Bits, Endian};
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use error::*;
use std::io::Cursor;

#[derive(Debug)]
pub enum Struct {
    I8(&'static str, i8),
    U8(&'static str, u8),
    I16(&'static str, i16),
    U16(&'static str, u16),
    I32(&'static str, i32),
    U32(&'static str, u32),
    I64(&'static str, i64),
    U64(&'static str, u64),
    Ihword(&'static str, i64), // half word
    Uhword(&'static str, u64), // half word
    Iword(&'static str, i64),
    Uword(&'static str, u64),
    Struct(&'static str, Vec<Struct>),
}

impl Struct {
    pub fn size(&self, bits: Bits) -> usize {
        let size = match self {
            &Struct::I8(_, _) => 1,
            &Struct::U8(_, _) => 1,
            &Struct::I16(_, _) => 2,
            &Struct::U16(_, _) => 2,
            &Struct::I32(_, _) => 4,
            &Struct::U32(_, _) => 4,
            &Struct::I64(_, _) => 8,
            &Struct::U64(_, _) => 8,
            &Struct::Ihword(_, _) => {
                match bits {
                    Bits::BITS_64 => 4,
                    Bits::BITS_32 => 2,
                }
            }
            &Struct::Uhword(_, _) => {
                match bits {
                    Bits::BITS_64 => 4,
                    Bits::BITS_32 => 2,
                }
            }
            &Struct::Iword(_, _) => {
                match bits {
                    Bits::BITS_64 => 8,
                    Bits::BITS_32 => 4,
                }
            }
            &Struct::Uword(_, _) => {
                match bits {
                    Bits::BITS_64 => 8,
                    Bits::BITS_32 => 4,
                }
            }
            &Struct::Struct(_, ref vals) => {
                let mut prev: Option<usize> = None;
                let mut size = 0;
                for i in vals {
                    match prev {
                        None => {}
                        Some(prev) => size += pad_size(prev, i, bits, size),
                    }
                    let s = i.size(bits);
                    prev = Some(s);
                    size += s;
                }
                size
            }
        };
        return size;
    }

    pub fn unpack(&mut self, bits: Bits, endian: Endian, data: &[u8]) -> Result<()> {
        match endian {
            Endian::LITTLE => {}
            _ => bail!{"only little endian supported."},
        }
        let mut buff = Cursor::new(data);
        match self {
            &mut Struct::I8(_, ref mut val) => *val = buff.read_i8()?,
            &mut Struct::U8(_, ref mut val) => *val = buff.read_u8()?,
            &mut Struct::I16(_, ref mut val) => *val = buff.read_i16::<LittleEndian>()?,
            &mut Struct::U16(_, ref mut val) => *val = buff.read_u16::<LittleEndian>()?,
            &mut Struct::I32(_, ref mut val) => *val = buff.read_i32::<LittleEndian>()?,
            &mut Struct::U32(_, ref mut val) => *val = buff.read_u32::<LittleEndian>()?,
            &mut Struct::I64(_, ref mut val) => *val = buff.read_i64::<LittleEndian>()?,
            &mut Struct::U64(_, ref mut val) => *val = buff.read_u64::<LittleEndian>()?,
            &mut Struct::Ihword(_, ref mut val) => {
                match bits {
                    Bits::BITS_32 => *val = buff.read_i16::<LittleEndian>()? as i64,
                    Bits::BITS_64 => *val = buff.read_i32::<LittleEndian>()? as i64,
                }
            }
            &mut Struct::Uhword(_, ref mut val) => {
                match bits {
                    Bits::BITS_32 => *val = buff.read_u16::<LittleEndian>()? as u64,
                    Bits::BITS_64 => *val = buff.read_u32::<LittleEndian>()? as u64,
                }
            }
            &mut Struct::Iword(_, ref mut val) => {
                match bits {
                    Bits::BITS_32 => *val = buff.read_i32::<LittleEndian>()? as i64,
                    Bits::BITS_64 => *val = buff.read_i64::<LittleEndian>()? as i64,
                }
            }
            &mut Struct::Uword(_, ref mut val) => {
                match bits {
                    Bits::BITS_32 => *val = buff.read_u32::<LittleEndian>()? as u64,
                    Bits::BITS_64 => *val = buff.read_u64::<LittleEndian>()?,
                }
            }
            &mut Struct::Struct(_, ref mut vals) => {
                let mut prev: Option<usize> = None;
                for ref mut i in vals {
                    match prev {
                        None => {}
                        Some(prev) => {
                            for _ in 0..pad_size(prev, i, bits, buff.position() as usize) {
                                buff.read_i8()?;
                            }
                        }
                    }
                    let pos = buff.position() as usize;
                    let size = i.size(bits);
                    i.unpack(bits, endian, &data[pos..(pos + size)])?;
                    buff.set_position((pos + size) as u64);
                    prev = Some(size);
                }
            }
        };
        if buff.position() as usize != data.len() {
            bail!{"too much data provided to unpack {}, unpacked {} bytes, got {}", self.get_name(), buff.position(), data.len()};
        }
        Ok(())
    }

    pub fn pack(&self, bits: Bits, endian: Endian) -> Result<Vec<u8>> {
        match endian {
            Endian::LITTLE => {}
            _ => bail!{"only little endian supported."},
        }
        let mut res: Vec<u8> = Vec::new();
        match self {
            &Struct::I8(_, val) => res.write_i8(val).unwrap(),
            &Struct::U8(_, val) => res.write_u8(val).unwrap(),
            &Struct::I16(_, val) => res.write_i16::<LittleEndian>(val).unwrap(),
            &Struct::U16(_, val) => res.write_u16::<LittleEndian>(val).unwrap(),
            &Struct::I32(_, val) => res.write_i32::<LittleEndian>(val).unwrap(),
            &Struct::U32(_, val) => res.write_u32::<LittleEndian>(val).unwrap(),
            &Struct::I64(_, val) => res.write_i64::<LittleEndian>(val).unwrap(),
            &Struct::U64(_, val) => res.write_u64::<LittleEndian>(val).unwrap(),
            &Struct::Ihword(_, val) => {
                match bits {
                    Bits::BITS_64 => res.write_i32::<LittleEndian>(val as i32).unwrap(),
                    Bits::BITS_32 => res.write_i16::<LittleEndian>(val as i16).unwrap(),
                }
            }
            &Struct::Uhword(_, val) => {
                match bits {
                    Bits::BITS_64 => res.write_u32::<LittleEndian>(val as u32).unwrap(),
                    Bits::BITS_32 => res.write_u16::<LittleEndian>(val as u16).unwrap(),
                }
            }
            &Struct::Iword(_, val) => {
                match bits {
                    Bits::BITS_64 => res.write_i64::<LittleEndian>(val).unwrap(),
                    Bits::BITS_32 => res.write_i32::<LittleEndian>(val as i32).unwrap(),
                }
            }
            &Struct::Uword(_, val) => {
                match bits {
                    Bits::BITS_64 => res.write_u64::<LittleEndian>(val).unwrap(),
                    Bits::BITS_32 => res.write_u32::<LittleEndian>(val as u32).unwrap(),
                }
            }
            &Struct::Struct(_, ref vals) => {
                let mut prev: Option<usize> = None;
                for i in vals {
                    match prev {
                        None => {}
                        Some(prev) => pad(prev, i, bits, &mut res),
                    }
                    prev = Some(i.size(bits));
                    res.append(&mut i.pack(bits, endian)?);
                }
            }
        }
        Ok(res)
    }

    fn get_name(&self) -> &str {
        match self {
            &Struct::I8(name, _) => name,
            &Struct::U8(name, _) => name,
            &Struct::I16(name, _) => name,
            &Struct::U16(name, _) => name,
            &Struct::U32(name, _) => name,
            &Struct::I64(name, _) => name,
            &Struct::I32(name, _) => name,
            &Struct::U64(name, _) => name,
            &Struct::Ihword(name, _) => name,
            &Struct::Uhword(name, _) => name,
            &Struct::Iword(name, _) => name,
            &Struct::Uword(name, _) => name,
            &Struct::Struct(name, _) => name,
        }
    }

    pub fn get(&self, name: &str) -> Result<&Struct> {
        match self {
            &Struct::Struct(_, ref vals) => {
                for i in vals {
                    if i.get_name() == name {
                        return Ok(i);
                    }
                }
            }
            _ => {}
        }
        bail!{"no entry with name {} found", name}
    }

    pub fn get_mut(&mut self, name: &str) -> Result<&mut Struct> {
        match self {
            &mut Struct::Struct(_, ref mut vals) => {
                for i in vals {
                    if i.get_name() == name {
                        return Ok(i);
                    }
                }
            }
            _ => {}
        }
        bail!{"no entry with name {} found", name}
    }

    pub fn set_u64(&mut self, val: u64) -> Result<()> {
        match self {
            &mut Struct::U8(_, ref mut v) => *v = val as u8,
            &mut Struct::U16(_, ref mut v) => *v = val as u16,
            &mut Struct::U32(_, ref mut v) => *v = val as u32,
            &mut Struct::U64(_, ref mut v) => *v = val,
            &mut Struct::Uhword(_, ref mut v) => *v = val,
            &mut Struct::Uword(_, ref mut v) => *v = val,
            &mut Struct::Struct(_, _) => bail!{"cannot set u64 value of struct"},
            _ => bail!{"cannot set u64 of unsigned value"},
        };
        Ok(())
    }

    pub fn set_i64(&mut self, val: i64) -> Result<()> {
        match self {
            &mut Struct::I8(_, ref mut v) => *v = val as i8,
            &mut Struct::I16(_, ref mut v) => *v = val as i16,
            &mut Struct::I32(_, ref mut v) => *v = val as i32,
            &mut Struct::I64(_, ref mut v) => *v = val,
            &mut Struct::Ihword(_, ref mut v) => *v = val,
            &mut Struct::Iword(_, ref mut v) => *v = val,
            &mut Struct::Struct(_, _) => bail!{"cannot set i64 value of struct"},
            _ => bail!{"cannot set i64 of signed value"},
        };
        Ok(())
    }

    pub fn to_u64(&self) -> Result<u64> {
        match self {
            &Struct::U8(_, val) => Ok(val as u64),
            &Struct::U16(_, val) => Ok(val as u64),
            &Struct::U32(_, val) => Ok(val as u64),
            &Struct::U64(_, val) => Ok(val),
            &Struct::Uhword(_, val) => Ok(val),
            &Struct::Uword(_, val) => Ok(val),
            &Struct::Struct(_, _) => bail!{"cannot get u64 value of struct"},
            _ => bail!{"cannot get u64 of unsigned value"},
        }
    }

    pub fn to_i64(&self) -> Result<i64> {
        match self {
            &Struct::I8(_, val) => Ok(val as i64),
            &Struct::U8(_, val) => Ok(val as i64),
            &Struct::I16(_, val) => Ok(val as i64),
            &Struct::U16(_, val) => Ok(val as i64),
            &Struct::I32(_, val) => Ok(val as i64),
            &Struct::U32(_, val) => Ok(val as i64),
            &Struct::I64(_, val) => Ok(val),
            &Struct::Ihword(_, val) => Ok(val),
            &Struct::Iword(_, val) => Ok(val),
            &Struct::Struct(_, _) => bail!{"cannot get i64 value of struct"},
            _ => bail!{"cannot get i64 of signed value"},
        }
    }
}

// current_size is the size of the struct up and with the previous node
fn pad_size(prev_size: usize, next: &Struct, bits: Bits, current_size: usize) -> usize {
    // TODO how are structs withing stucts aligned?
    if prev_size == next.size(bits) {
        return 0;
    }
    // TODO always align to word size?
    let word = Struct::Uword("", 0).size(bits);
    let rem = current_size % word;
    if rem == 0 {
        return 0;
    }
    return word - rem;
}

fn pad(prev_size: usize, next: &Struct, bits: Bits, res: &mut Vec<u8>) {
    for _ in 0..pad_size(prev_size, next, bits, res.len()) {
        res.push(0);
    }
}
