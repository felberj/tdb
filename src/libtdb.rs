use binary::{Bits, Elf, Endian, Machine};
use capstone;
use error::*;
use file;
use libkernel::{Amd64Kernel, Kernel, X86Kernel};
use libload::Process;
use std::{collections, ffi, mem, thread};
use std::sync::{mpsc, Arc};
use unicorn::{Arch, CodeHookType, InsnSysX86, MemHookType, Mode, RegisterX86, Unicorn};

pub type Cpu = Unicorn<System>;

// Holder class
pub struct System {
    pub process: Process,
    pub kernel: Box<Kernel>,
    pub event_transmitter: mpsc::SyncSender<EmulationEvent>,
    pub time: u64,
}

type ExecutionCallback = Box<FnMut(&Instruction) + Send>;
type SyscallCallback = Box<FnMut(u64, String) + Send>;

#[repr(C)]
pub struct Tdb {
    pub cpu: Box<Cpu>,
    pub sys: System,
    pub ecb: Option<ExecutionCallback>,
    pub scb: Option<SyscallCallback>,
}

#[derive(Default)]
#[repr(C)]
pub struct Options {
    pub program_arguments: Vec<String>,
    pub cpu: Option<Box<Cpu>>,
    pub process: Option<Process>,
    pub ecb: Option<ExecutionCallback>,
    pub scb: Option<SyscallCallback>,
}

pub struct Instruction {
    pub id: u32,                                 // Instruction id
    pub time: u64,                               // Time when the instruction was executed
    pub address: u64,                            // Instruction address
    pub mnemonic: String,                        // The mnemonic for the instruction
    pub op: String,         // The operand string associated with the instruction
    pub repr: ffi::CString, // String representation
    pub regs: collections::HashMap<String, u64>, // Registers before the instruction was executed
}

impl Instruction {
    fn from_capstone(
        time: u64,
        ins: capstone::Insn,
        registers: collections::HashMap<String, u64>,
    ) -> Instruction {
        let mm = ins.mnemonic().map_or(String::new(), |s| s.to_string());
        let op = ins.op_str().map_or(String::new(), |s| s.to_string());
        let repr = format!{"{} {}", mm, op};
        Instruction {
            time: time,
            id: ins.id(),
            address: ins.address(),
            mnemonic: mm,
            op: op,
            repr: ffi::CString::new(repr).unwrap(), // FIXME unwrap
            regs: registers,
        }
    }
}

pub enum EmulationEvent {
    ExecutedCode {
        addr: u64,
        instruction: Vec<u8>,
        time: u64,
        registers: collections::HashMap<String, u64>,
    },
    ExecutedSyscall {
        time: u64,
        name: String,
    },
    MemoryRead {
        addr: u64,
        value: Vec<u8>,
    },
    MemoryWrite {
        addr: u64,
        value: Vec<u8>,
    },
    RegisterRead {
        reg: RegisterX86,
        value: u64,
    },
    RegisterWrite {
        reg: RegisterX86,
        value: u64,
    },

    QueryExecution {
        from: usize,
        until: usize,
        ret: mpsc::SyncSender<Arc<Instruction>>,
    },
}

impl Tdb {
    pub fn new(elf: &Elf, options: Options) -> Result<Tdb> {
        let mut cpu = match options.cpu {
            Some(c) => c,
            None => match elf.ehdr.machine {
                Machine::X86 => Unicorn::new(Arch::X86, Mode::MODE_32)?,
                Machine::X86_64 => Unicorn::new(Arch::X86, Mode::MODE_64)?,
            },
        };
        // TODO make sure that arch and endian is correct!
        let mut process = match options.process {
            Some(c) => c,
            None => match elf.ehdr.machine {
                Machine::X86 => Process {
                    brk: 0x55550000,
                    internal_brk: 0x65550000,
                    bits: Bits::BITS_32,
                    endian: Endian::LITTLE,
                    program_arguments: options.program_arguments,
                    stdout: String::new(),
                    fs: file::System::new(),
                    entry_point: None,
                    gdt: None,
                    thread_areas: Default::default(),
                    memory_names: Default::default(),
                },
                Machine::X86_64 => Process {
                    brk: 0x5555555500000000,
                    internal_brk: 0x6555555500000000,
                    bits: Bits::BITS_64,
                    endian: Endian::LITTLE,
                    program_arguments: options.program_arguments,
                    stdout: String::new(),
                    fs: file::System::new(),
                    gdt: None,
                    entry_point: None,
                    thread_areas: Default::default(),
                    memory_names: Default::default(),
                },
            },
        };
        let mut k: Box<Kernel> = match elf.ehdr.machine {
            Machine::X86 => Box::new(X86Kernel {
                syscalls: collections::HashMap::new(),
            }),
            Machine::X86_64 => Box::new(Amd64Kernel {
                syscalls: collections::HashMap::new(),
            }),
        };
        k.init(&mut process, &mut cpu)?;
        let (tx, rx) = mpsc::sync_channel::<EmulationEvent>(0);
        let mut t = Tdb {
            sys: System {
                kernel: k,
                process: process,
                event_transmitter: tx,
                time: 0,
            },
            cpu: cpu,
            ecb: options.ecb,
            scb: options.scb,
        };
        t.start_analyzer(rx);
        t.set_hooks();
        Ok(t)
    }

    pub fn start(&mut self) -> Result<()> {
        let start = match self.sys.process.entry_point {
            Some(s) => s,
            None => bail!{"no entry point set"},
        };
        self.cpu.emu_start(start, 0, 0, 0, &mut self.sys)?;
        Ok(())
    }

    fn start_analyzer(&mut self, rx: mpsc::Receiver<EmulationEvent>) {
        let bits = self.sys.process.bits;
        let mut ecb = mem::replace(&mut self.ecb, None);
        let mut scb = mem::replace(&mut self.scb, None);
        thread::spawn(move || {
            let cs = match bits {
                Bits::BITS_32 => capstone::Capstone::new_raw(
                    capstone::Arch::X86,
                    capstone::Mode::Mode32,
                    capstone::NO_EXTRA_MODE,
                    None,
                ).unwrap(),
                Bits::BITS_64 => capstone::Capstone::new_raw(
                    capstone::Arch::X86,
                    capstone::Mode::Mode64,
                    capstone::NO_EXTRA_MODE,
                    None,
                ).unwrap(),
            };
            let mut execution = Vec::new();
            for event in rx.iter() {
                match event {
                    EmulationEvent::ExecutedCode {
                        addr,
                        instruction: ins,
                        time,
                        registers,
                    } => {
                        let res = cs.disasm_count(ins.as_slice(), addr, 1).unwrap();
                        for i in res.iter() {
                            let instr = Arc::new(Instruction::from_capstone(time, i, registers));
                            execution.push(instr.clone());
                            match ecb {
                                Some(ref mut callback) => callback(&instr),
                                None => {}
                            }
                            break; // TODO more elegant
                        }
                    }
                    EmulationEvent::ExecutedSyscall { name, time } => match scb {
                        Some(ref mut callback) => callback(time, name),
                        None => {}
                    },
                    _ => println!{"received unknown event"},
                }
            }
        });
    }

    fn set_hooks(&mut self) {
        self.cpu
            .add_code_hook(
                CodeHookType::CODE,
                0,
                0xffffffffffffffff,
                |cpu: &mut Cpu, s: &mut System, addr, len| {
                    let inst = cpu.mem_read(addr, len as usize).unwrap();
                    let regs = s.kernel.collect_registers(cpu).unwrap();
                    s.event_transmitter
                        .send(EmulationEvent::ExecutedCode {
                            addr: addr,
                            instruction: inst,
                            time: s.time,
                            registers: regs,
                        })
                        .unwrap();
                    s.time += 1;
                },
            )
            .unwrap();
        self.cpu
            .add_mem_hook(
                MemHookType::MEM_UNMAPPED,
                0,
                0xffffffffffffffff,
                |_u: &mut Cpu, _s: &mut System, _t, addr, len, val| {
                    let sp = _u.reg_read(RegisterX86::ESP).unwrap();

                    println!(
                        "MEM UNMAPPED: addr 0x{:x}, len {}, val {:x}, sp: {:x}",
                        addr, len, val, sp
                    );
                    false
                },
            )
            .unwrap();
        self.cpu
            .add_mem_hook(
                MemHookType::MEM_PROT,
                0,
                0xffffffffffffffff,
                |_u: &mut Cpu, _s: &mut System, t, addr, len, val| {
                    println!(
                        "MEM PROT: addr 0x{:x}, len {}, val {:x}, t: {:?}",
                        addr, len, val, t
                    );
                    false
                },
            )
            .unwrap();
        self.cpu
            .add_intr_hook(|u: &mut Cpu, s: &mut System, intnr| {
                if intnr == 0x80 {
                    let call = s.kernel.handle_syscall(&mut s.process, u).unwrap();
                    s.event_transmitter
                        .send(EmulationEvent::ExecutedSyscall {
                            time: s.time - 1,
                            name: call,
                        })
                        .unwrap();
                } else if intnr == 0x0D {
                    panic!{"general protection fault"};
                } else {
                    panic!{"unknown interrupt {}", intnr};
                }
            })
            .unwrap();
        self.cpu
            .add_insn_sys_hook(
                InsnSysX86::SYSCALL,
                1,
                0,
                |u: &mut Unicorn<System>, s: &mut System| {
                    let call = s.kernel.handle_syscall(&mut s.process, u).unwrap();
                    s.event_transmitter
                        .send(EmulationEvent::ExecutedSyscall {
                            time: s.time - 1,
                            name: call,
                        })
                        .unwrap();
                },
            )
            .unwrap();
    }
}
