import subprocess
import re

readelf = 'greadelf' # osx, sorry

def type_to_enum(t):
    if t == 'PHDR':
        return 'Phdr'
    if t == 'INTERP':
        return 'Interp'
    if t == 'LOAD':
        return 'Load'
    if t == 'DYNAMIC':
        return 'Dynamic'
    if t == 'NOTE':
        return 'Note'
    if t == 'GNU_EH_FRAME':
        return 'GnuEhFrame'
    if t == 'GNU_STACK':
        return 'GnuStack'
    if t == 'TLS':
        return 'Tls'
    if t == 'GNU_RELRO':
        return 'GnuRelro'
    print t
    raise t

def generate_test(i, f):
    out = subprocess.check_output([readelf, '-l', f]) 
    out = out.split('Program Headers:\n')[1].split('\n Section to Segment mapping:')[0]
    out = re.sub("[ ]+", " ", out)
    out = re.sub("R E", "RE", out)
    double_line = True
    index = 0
    print("TestCase {file: \"%s\",want: vec!(" % f)
    while out.strip() != '':
        [line, out] = out.split('\n', 1)
        if '[' in line:
            # todo parse loader/interpreter
            continue
        if 'FileSiz MemSiz Flg Align' in line:
            double_line = False

        line = line.strip()
        if double_line:
            [line2, out] = out.split('\n', 1) 
            line = line + ' ' + line2.strip()
        if 'Type Offset VirtAddr PhysAddr' in line:
            continue  
        [type_, offset, virtaddr, physaddr, filesize, memsize, flags, align] = line.split(' ')
        if not '0x' in align:
            align = '0x' + align

        readable = 'true' if 'R' in flags else 'false'
        writable = 'true' if 'W' in flags else 'false'
        executable = 'true' if 'E' in flags else 'false'
        print("Phdr{p_type: SegmentType::%s,p_offset: %s,p_filesz: %s,p_vaddr: %s,p_memsz: %s ,p_align: %s,  readable: %s, writable: %s, executable: %s, }," % (type_to_enum(type_), offset, filesize, virtaddr, memsize, align, readable, writable, executable))
        
    print('),},')

binaries = ['testdata/elf_x86_64_keyval.out', 'testdata/static/static_x86', 'testdata/static/static_x86_64', 'testdata/lib64/ld-linux-x86-64.so.2']

for i, f in enumerate(binaries):
    generate_test(i, f)
