extern crate tdb;
extern crate unicorn;
#[macro_use]
extern crate error_chain;

use binary::SegmentType;
use libtest::T;
use std::fs::File;
use std::io::prelude::*;
use tdb::*;

#[test]
fn test_parse_interpreter() {
    let expected = "/lib64/ld-linux-x86-64.so.2";
    let elf_path = "testdata/elf_x86_64_keyval.out";
    let mut file = File::open(elf_path).unwrap();
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).unwrap();
    let elf = binary::parse_elf(contents).unwrap();

    libload::parse_elf_interp(&elf,
                              &|path: &str| {
        if path != expected {
            bail!("Invalid path: {:?}", path);
        }
        let mut interp_vec: Vec<u8> = Vec::new();
        let mut file = File::open(elf_path).unwrap();
        file.read_to_end(&mut interp_vec)?;
        Ok(interp_vec)
    })
        .unwrap();
}

#[test]
fn test_load_elf() {
    let mut t: T = Default::default();
    let page_size = 0x1000;
    let elf_path = "testdata/static/static_x86_64";
    let mut file = File::open(elf_path).unwrap();
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).unwrap();
    let elf = binary::parse_elf(contents).unwrap();
    let tdb = libtdb::Tdb::new(&elf, Default::default()).unwrap();

    let mut p = tdb.sys.process;
    let mut cpu = tdb.cpu;
    libload::load_elf(&mut p, &mut cpu, &elf).unwrap();
    let regions = cpu.mem_regions().unwrap();
    for p in &elf.phdrs {
        if p.p_type != SegmentType::Load {
            continue;
        }
        let mut normalised = p.p_vaddr & !(page_size - 1);
        while normalised < p.p_vaddr + p.p_memsz {
            if !regions.iter().any(|r| r.begin == normalised) {
                t.error(format!("page {} not in memory", normalised));
            }
            normalised += page_size;
        }
    }
}

#[test]
fn test_correctly_set_arguments() {
    let mut t: T = Default::default();

    let elf_path = "testdata/static/static_x86_64_print_args";
    let mut file = File::open(elf_path).unwrap();
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).unwrap();

    let elf = binary::parse_elf(contents).unwrap();
    let options = libtdb::Options { program_arguments: vec![String::from("one"), String::from("two")], ..Default::default() };
    let mut tdb = libtdb::Tdb::new(&elf, options).unwrap();
    libload::load_elf(&mut tdb.sys.process, &mut tdb.cpu, &elf).unwrap();
    tdb.start().unwrap();
    let expected = "arg 0 ./my_executable
arg 1 one
arg 2 two
";
    if expected != tdb.sys.process.stdout {
        t.error(format!{"Wrong stdout: Expected {}, got {}", expected, tdb.sys.process.stdout})
    }
}

#[test]
fn test_correctly_set_arguments_32() {
    let mut t: T = Default::default();

    let elf_path = "testdata/static/static_x86_print_args";
    let mut file = File::open(elf_path).unwrap();
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).unwrap();

    let elf = binary::parse_elf(contents).unwrap();
    let options = libtdb::Options { program_arguments: vec![String::from("one"), String::from("two")], ..Default::default() };
    let mut tdb = libtdb::Tdb::new(&elf, options).unwrap();
    libload::load_elf(&mut tdb.sys.process, &mut tdb.cpu, &elf).unwrap();
    tdb.start().unwrap();
    let expected = "arg 0 ./my_executable
arg 1 one
arg 2 two
";
    if expected != tdb.sys.process.stdout {
        t.error(format!{"Wrong stdout: Expected {}, got {}", expected, tdb.sys.process.stdout})
    }
}
