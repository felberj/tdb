
#ifndef cheddar_generated_libtdb_h
#define cheddar_generated_libtdb_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>



typedef Unicorn Cpu;

typedef struct Tdb {
	Box cpu;
	System sys;
	Option ecb;
	Option scb;
} Tdb;

typedef struct Options {
	Vec program_arguments;
	Option cpu;
	Option process;
	Option ecb;
	Option scb;
} Options;



#ifdef __cplusplus
}
#endif


#endif
